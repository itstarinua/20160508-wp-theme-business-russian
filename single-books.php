<?php
/********************************************
*********************************************
Description: Шаблон вывода записей типа Книга
Author: DStaroselskiy (it.star.varvar@gmail.com)
Author URI: https://plus.google.com/u/0/110295925295050770002/posts
Version: 0.1
Date: 04/06/2016
*********************************************
********************************************/

get_header(); 

$page_header_bg_style = "";
$exclude_book = 0;
if ( have_posts() ) {
	while ( have_posts() ) { 
		the_post();
		$exclude_book = get_the_ID();
		$BOOK_PARAMETERS = get_post_meta(  $exclude_book , '_BOOK_PARAMETERS', true);
		?>
		<div id="book-description-container">	
			<div id="book-description-row" class="theme-container">
				<div id="book-image-container"> 
					<div id="book-image">
						<?php if( has_post_thumbnail() ) { 
							the_post_thumbnail( 'full' ); 
						} else { ?>
							<div class="attachment-book size-book wp-post-image no-photo-225x291"></div>						
						<?php } ?>
					</div>	
					<ul class="book-costs">
						<?php if( !empty( $BOOK_PARAMETERS['cost_val'] ) ) { ?> 
						<li id="book-cost" class="<?php echo (!empty( $BOOK_PARAMETERS['cost_akcia_val']) ? ' promo' : '');?>">
							<?php echo $BOOK_PARAMETERS['cost_val'];?>
						</li>
						<?php } 
						if( !empty( $BOOK_PARAMETERS['cost_akcia_val'] ) ) { ?> 
							<li id="book-cost-akcia ">
								<?php echo $BOOK_PARAMETERS['cost_akcia_val'];?>
							</li>
						<?php } ?>
					</ul>
				</div>
					<h1 id="book-title">
						<?php the_title(); ?>
					</h1>
					<h3 id="book-excerpt">
						<?php echo $BOOK_PARAMETERS['level_val']; ?>
					</h3>
					<div id="book-voting-container">
							<?php if( function_exists( 'psr_show_voting_stars' ) ) psr_show_voting_stars(); ?>
					</div>
					<div id="book-details-container">
						<div id="book-details">
							<div>
								<h4 class="book-detail-title theme-page-h2"><?php _e('Product details','dms-business-russian');?></h4>
							</div>
							<table> 
							<?php if( !empty($BOOK_PARAMETERS['file_size_val']) ) { ?>
								<tr><td><?php _e('Размер файла','dms-business-russian');?>:</td><td><?php echo $BOOK_PARAMETERS['file_size_val'];?></td></tr>	
							<?php } 
							if( !empty($BOOK_PARAMETERS['pages_count_val']) ) { ?>
								<tr><td><?php _e('Length','dms-business-russian');?>:</td><td> <?php echo $BOOK_PARAMETERS['pages_count_val'];?></td></tr>
							<?php } 
							if( !empty($BOOK_PARAMETERS['publisher_val']) ) { ?>
								<tr><td><?php _e('Издательство','dms-business-russian');?>:</td><td> <?php echo $BOOK_PARAMETERS['publisher_val'];?></td></tr>
							<?php } ?>
							</table>
						</div>
						<!-- noindex -->
						<?php if( isset($BOOK_PARAMETERS['paypal_secret_action']) 
								&& !empty($BOOK_PARAMETERS['paypal_secret_action']) 
								&& isset($_REQUEST['action']) 
								&& !empty($_REQUEST['action']) 
								&& ( $BOOK_PARAMETERS['paypal_secret_action'] == $_REQUEST['action'] ) 
						) { ?>
							<div class="btn-pay-conteiner">
								<a class="btn-pay"rel="nofollow"  target="_blank" href="<?php echo $BOOK_PARAMETERS['link_for_content_load'];?>" title="<?php echo sprintf(__('Скачать материал %s','dms-business-russian'),get_the_title());?>">
									<i class="fa fa-download" aria-hidden="true"></i>   
									<?php _e('Download','dms-business-russian'); ?>
								</a>	
							</div>	
						<?php }else{
							if( !empty($BOOK_PARAMETERS['payed_market_val']) ) { ?>
							<div class="btn-pay-conteiner">
								<a class="btn-pay" rel="nofollow" target="_blank" href="<?php echo $BOOK_PARAMETERS['payed_market_val'];?>" title="<?php echo sprintf(__('Купить %s на Amazon','dms-business-russian'),get_the_title());?>">
									<i class="fa fa-shopping-cart" aria-hidden="true"></i>    
									<?php _e('Amazon','dms-business-russian'); ?>
								</a>	
							</div>
						<?php } ?>	
						<?php if( !empty($BOOK_PARAMETERS['paypal_hosted_button_id']) ) { ?>
							<form action="https://www.paypal.com/cgi-bin/webscr" target="_blank" method="post" target="_top" style="text-align:center;display: block; margin-left: auto; margin-right: auto;">
								<input name="cmd" value="_s-xclick" type="hidden">
								<input name="hosted_button_id" value="<?php echo $BOOK_PARAMETERS['paypal_hosted_button_id'];?>" type="hidden">								
								<!--input src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" name="submit" alt="PayPal - The safer, easier way to pay online!" type="image" border="0"-->								
								<button class="btn-pay" type="submit">	
									<i class="fa fa-shopping-cart" aria-hidden="true"></i>    
									<?php _e('PayPal','dms-business-russian'); ?>
								</button>
								<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
							</form>
							<?php /*
							<!--div class="btn-pay-conteiner">
								<a class="btn-pay" href="<?php echo $BOOK_PARAMETERS['payed_market_paypal_val'];?>" title="<?php echo sprintf(__('Купить %s на PAYPAL','dms-business-russian'),get_the_title());?>">
									<i class="fa fa-shopping-cart" aria-hidden="true"></i>    
									<?php _e('PayPal','dms-business-russian'); ?>
								</a>	
							</div-->
							*/ ?>
						<?php } 	
						} ?>		
						<!-- /noindex -->
					</div>
					<div id="book-content">
						<?php the_content(); ?>
					</div>					

			</div>
		</div>	
		<div id="page-body">
			<div id="left-container">
			<?php if( (int)$BOOK_PARAMETERS['author'] > 0 ) { 
				$autors = get_posts( array(
					'include' => (int)$BOOK_PARAMETERS['author'],
					'post_type' 	=> 'our-author',
					'post_status' 	=> 'any',
				) );
				if( count($autors) > 0 ) { 
					foreach( $autors as $autor ) { 
						setup_postdata( $autor ); 
						if( has_post_thumbnail($autor->ID) ) { ?>
							<div id="about-author" class="is-author-photo">
								<?php echo get_the_post_thumbnail($autor->ID, 'author', array( 'class' => "attachment attachment-author author-photo") ); 
						} else { ?>
							<div id="about-author">
						<?php } ?>
								<div class="author-info">
									<div>
										<h3 class="author-title theme-page-h2"><?php _e('Об авторе','dms-business-russian');?></h3>
									</div>
									<div class="author-description">
										<?php the_content();?>
									</div>
								</div>
							</div>	
					<?php } 
					wp_reset_postdata();
				} 
			}
			$books = get_posts( array(
				'exclude' 		=> $exclude_book,
				'post_type'		=> 'books',
				'numberposts' 	=> 4,					
			) );
			if( count($books) > 0 ) { ?>
				<div class="books-title">
					<h3 class="theme-page-h2"><?php _e('Наши книги','dms-business-russian');?></h3>
				</div>
				<div id="books-list"> 							
					<?php foreach( $books as $book ) { 
						setup_postdata( $book ); ?>
						<div class="book-information">
							<a href="<?php the_permalink($book->ID);?>" title="<?php echo sprintf( __("Читать о %s","dms-business-russian"), $book->post_title );?>">
								<?php if( has_post_thumbnail($book->ID) ) { ?>
									<?php echo get_the_post_thumbnail($book->ID, 'book-small', array( 'class' => "attachment attachment-book book-small") ); 
								} else { ?>
									<div class="attachment-book size-book wp-post-image no-photo-133x169"></div>
								<?php } ?>
								<h6 class="book-title">
									<?php echo $book->post_title; ?>
								</h6>
								<?php $BOOK_PARAMETERS = get_post_meta(  $book->ID , '_BOOK_PARAMETERS', true); 
								if( !empty( $BOOK_PARAMETERS['level_val'] ) ) { ?>
									<div class="book-level">
										<?php echo $BOOK_PARAMETERS['level_val']; ?>
									</div>
								<?php } ?>
							</a>
						</div>	
					<?php } ?>
				</div>					
				<?php wp_reset_postdata();
			} 
			if ( comments_open( $exclude_book ) ) {	

				$comments = get_comments( array(
					'post_id' => $exclude_book,
					'orderby' => 'comment_date_gmt',
					'order' => 'ASC',
					'status' => 'approve',
				) ); ?>
				<div id="comments-list" > 						
					<div class="comments-title">
						<h3 class="theme-page-h2"><?php _e('Комментарии','dms-business-russian');?></h3>
					</div>
					<?php foreach( $comments as $comment ) {  ?>
						<div class="comment-information feedback-container">
							<?php echo $comment->comment_content; ?>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
			</div>
			<ul id="sidebar-right">
					<li>
						<h5 class="widgettitle"><?php _e('Категории','dms-business-russian');?></h5>
						<?php $post_term_list = wp_get_post_terms($exclude_book, 'category_of_books', array("fields" => "all"));
						$category_of_books = get_terms( 'category_of_books', array(
							'taxonomy'      => 'books',
							'orderby'       => 'name', 
							'order'         => 'ASC',
							'hide_empty'    => false, 
							'fields'        => 'all', 
							'hierarchical'  => false, 
							'get'           => 'all',
							'pad_counts'    => false, 
							'update_term_meta_cache' => true,
						) ); ?>
						<ul class="terms-lists">
							<?php foreach ($category_of_books as $category) { 
								$term_select = "";
								if( count($post_term_list) > 0 ) {
									foreach ($post_term_list as $post_term) {
										if( $post_term->term_id == $category->term_id ) $term_select = "term-active";
									}
								} ?>
								<li class="terms term_id-<?php echo  $category->term_id;?> term_slug-<?php echo  $category->slug;?> <?php echo $term_select ;?>"><a href="<?php echo get_term_link( $category->term_id, 'category_of_books' );?>" title="<?php echo $category->name;?>"><?php echo $category->name;?></a></li>
							<?php } ?>
						</ul>	
					</li>
					<?php if ( is_active_sidebar( 'sidebar-content-right' ) ) { dynamic_sidebar( 'sidebar-content-right' ); } ?>
				</ul>
			</div>
			<div style="clear:both;"></div>	
		</div>
	<?php }
}else{
	echo  '<div id="page-body">';
	get_template_part( 'content','404' );
	echo '</div> <!-- End of #page-body -->';
}

get_footer(); 
?>