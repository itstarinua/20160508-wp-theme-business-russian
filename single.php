<?php
/********************************************
*********************************************
Description: Основной шаблон вывода данных
Author: DStaroselskiy (it.star.varvar@gmail.com)
Author URI: https://plus.google.com/u/0/110295925295050770002/posts
Version: 0.1
Date: 22/05/2016
*********************************************
********************************************/
get_header(); 

if ( have_posts() ) {
	while ( have_posts() ) { 
		the_post(); 
		$the_ID = get_the_ID();?>
		<div class="single-title-container">
			<h1 class="single-title theme-container"><?php the_title();?></h1>
			<ul class="main-single-info theme-container">
				<?php 
					$categorys = get_the_category();
					if( count($categorys) > 0 ) foreach ($categorys as $category) { ?>
						<li class="post-category post-category-id-<?php echo $category->cat_ID;?>>"><?php echo $category->cat_name;?></li>
					<?php } ?>
					<li class="post-views"><?php echo apply_filters('show_post_views', $the_ID ) ?></li>
					<?php if( function_exists( 'psr_show_voting_stars' ) ) { ?>
						<li class="post-raiting"><?php psr_show_voting_stars(); ?></li>
					<?php }	?>
			</ul>
		</div>
		<div id="page-body"> 
			<div id="left-container">	
				<?php $AUTHOR_ID = (int)get_post_meta( $the_ID, '_AUTHOR_ID', true);
				if( !empty( $AUTHOR_ID ) ) { 
					$dms_posts = new \WP_Query(); 
					$authors = $dms_posts->query( array(
						'post_type' 	=> 'our-author',
						'p' => $AUTHOR_ID,
						'post_status' 	=> 'any',
						'posts_per_page' => -1,			
					)); 
					if( isset( $authors[0] ) ) { ?>
						<div class="post-author">
							<?php if( has_post_thumbnail( $authors[0]->ID ) ) echo get_the_post_thumbnail( $authors[0]->ID , 'avatar-small', array( 'class'=>'attachment-avatar-small author-avatar', 'alt'=> trim(strip_tags( $authors[0]->post_name )), 'title'=> trim(strip_tags( $authors[0]->post_title )), ) );?>
							<div class="autor-name"><?php echo $authors[0]->post_title;?></div>
						</div>
					<?php }
				} ?>
				<div class="single-content">
					<?php if( has_post_thumbnail( $the_ID ) ) echo get_the_post_thumbnail( $the_ID , 'full', array( 'class'=>'attachment-single', 'width'=>"240",'alt'=> '', 'title'=> '', ) );?>
					<?php the_content(); ?>
				</div>
				<div class="single-share">
					<div class="table">
						<div class="tr">
							<div class="td"><?php _e('Понравилось? Расскажите друзьям:','dms-business-russian');?></div>
							<div class="td"><iframe src="https://www.facebook.com/plugins/like.php?href=<?php echo urlencode(get_permalink());?>&width=215&layout=button_count&action=like&show_faces=true&share=true&height=46&appId" width="215" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></div>
						</div>
						<div class="tr">
							<div class="td"><?php _e('Оцените статью:','dms-business-russian');?></div>
							<div class="td"><?php if( function_exists( 'psr_show_voting_stars' ) ) psr_show_voting_stars(); ?></div>
						</div>
					</div>
				</div>
                                                            <?php 
                                   global $post;
                                    $search_arg = array(
                                        'posts_per_page' => -1, // Количество вывода похожих статей.
                                        'ignore_sticky_posts' => 1,
                                        'post__not_in' => array( (int)$post->ID ),  
                                        'post_status'  => "publish",
                                        'post_type' => $post->post_type,
                                    );    
                                    $terms = get_the_terms( (int)$post->ID, 'category');
                                    
                                    if( is_array( $terms ) && ( count( $terms ) > 0  ) ) 
                                    {
                                        $terms_array = array();
                                        
                                        foreach($terms as $term) 
                                        {
                                            $terms_array[] = $term->term_id;
                                        }
                                        $search_arg['tax_query'] = array(
                                                'relation' => 'AND',
                                                array(
                                                        'taxonomy' => 'category',
                                                        'field'    => 'id',
                                                        'terms'    => $terms_array,
                                                        'operator' => 'IN',
                                                )
                                        );
                                    }       
                                    $my_query = new \WP_Query( $search_arg );
                                    
                                    if( $my_query->have_posts() ) 
                                    {                                           
                                        
                                        $post_i = array( );
                                        if( $my_query->post_count > 10 )
                                        {
                                            $i = 0;
                                            while( $i < 3 )
                                            {
                                                $roun = rand(0, ( $my_query->post_count - 1) );
                                                $ok = true;
                                                foreach( $post_i as $pi )
                                                {
                                                    if( $pi == $roun ) 
                                                    {
                                                        $ok = false; 
                                                        break;
                                                    }
                                                }
                                                if( $ok )
                                                {
                                                   $post_i[] = $roun;
                                                   $i = $i + 1;
                                                }
                                            }
                                        }
                                        else 
                                        {
                                            $end = ( $my_query->post_count < 3 ) ? $my_query->post_count : 3;                                            
                                            for( $i=0; $i < $end ; $i++ )
                                            {
                                                $post_i[] = $i;
                                            }
                                        }
                                        //global $post;
                                        //$backpost = $post;
                                        ?>
                                        <!-- noindex -->
                                        <div>
                                            <h3 class="theme-page-h2"><?php _e('More interesting posts');?></h3>
                                        </div>
                                        <div class="category-content related-posts">
                                            <?php foreach( $post_i as $i ) {
                                                //$my_query->the_post();
                                               // setup_postdata( $my_query->posts[ $i ] );
                                                
                                                $related_posts_ID = $my_query->posts[ $i ]->ID;  ?>
                                                <a class="post-info" href="<?php echo get_permalink( $my_query->posts[ $i ] ); ?>" rel="nofollow" title="<?php echo sprintf(__('Читать далее %s','dms-business-russian'),the_title())?>">
                                                        <div class="category-img-content">
                                                                <?php if( has_post_thumbnail( $the_ID ) ) {
                                                                        echo get_the_post_thumbnail( $related_posts_ID , 'categiry-small', array( 'class'=>'attachment-category', 'alt'=> '', 'title'=> '', ) );
                                                                }else{
                                                                        ?><div class="attachment-category wp-post-image no-photo-279x181"></div><?php						
                                                                } ?>
                                                        </div>
                                                        <?php $POSTS_VIEWS = (int)get_post_meta( $related_posts_ID, '_POSTS_VIEWS', true);
                                                        if( empty($POSTS_VIEWS) ) $POSTS_VIEWS = 0; ?>
                                                        <div class="post-views"><?php echo $POSTS_VIEWS; ?></div>
                                                        <?php $post_term_list_rp = wp_get_post_terms($related_posts_ID, 'category', array("fields" => "all")); ?>
                                                        <ul class="post-category-lists">
                                                                <?php if( count($post_term_list_rp) > 0 ) {
                                                                        foreach ($post_term_list_rp as $post_term) {
                                                                                ?><li class="terms term_id-<?php echo  $post_term->term_id;?> term_slug-<?php echo  $post_term->slug;?>"><?php echo $post_term->name;?></li><?php
                                                                        }
                                                                } ?>				
                                                        </ul>	
                                                        <h3 class="post-title"><?php echo get_the_title( $my_query->posts[ $i ] );?></h3>
                                                        <div class="post-excerpt"><?php echo get_the_excerpt( $my_query->posts[ $i ] );?></div>
                                                        <div class="post-link"><span><?php _e('Далее','dms-business-russian') ?> <i class="typcn typcn-arrow-right"></i></span></div>
                                                </a>		
                                            <?php } 
                                            //$post = $backpost;
                                            //wp_reset_postdata(); 
                                            ?>
                                        </div>	
                                        <!-- /noindex -->
                                    <?php } ?>

                                <div>
					<h3 class="theme-page-h2">News & Special Offers</h3>
				</div>
				<div class="subscribe-form">
					<?php get_getresponse_form(); ?>
					<p  class="subscribe-footer"><?php _e('Мы против спама. Ваши данные никогда не будут переданы 3-м лицам.','dms-business-russian');?></p>
				</div>
			</div>
			<ul id="sidebar-right">
				<li>
					<h5 class="widgettitle"><?php _e('Категории','dms-business-russian');?></h5>
					<?php $post_term_list = wp_get_post_terms($the_ID, 'category', array("fields" => "all"));
					$categorys = get_terms( 'category', array(
						'taxonomy'      => 'post',
						'orderby'       => 'name', 
						'order'         => 'ASC',
						'hide_empty'    => false, 
						'fields'        => 'all', 
						'hierarchical'  => false, 
						'get'           => 'all',
						'pad_counts'    => false, 
						'update_term_meta_cache' => true,
					) ); ?>
					<ul class="terms-lists">
						<?php foreach ($categorys as $category) { 
							$term_select = "";
							if( count($post_term_list) > 0 ) {
								foreach ($post_term_list as $post_term) {
									if( $post_term->term_id == $category->term_id ) $term_select = "term-active";
								}
							} ?>
							<li class="terms term_id-<?php echo  $category->term_id;?> term_slug-<?php echo  $category->slug;?> <?php echo $term_select ;?>"><a href="<?php echo get_term_link( $category->term_id, 'category_of_books' );?>" title="<?php echo $category->name;?>"><?php echo $category->name;?></a></li>
						<?php } ?>
					</ul>	
				</li>
				<?php if ( is_active_sidebar( 'sidebar-content-right' ) ) { dynamic_sidebar( 'sidebar-content-right' ); } ?>
			</ul>
			<div style="clear:both;"></div>	
		</div>		
	<?php }
}else{
	echo  '<div id="page-body">';
	get_template_part( 'content','404' );
	echo '</div> <!-- End of #page-body -->';
}
get_footer(); 
?>