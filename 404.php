<?php get_header(); ?>

<div id="page-body" class="page404">
	<h1>404 - error</h1> 
	<h2>The page you requested cannot be found.</h2>
	<h3><a href="<?php echo home_url();?>" title="Go home">Go home page</a></h3>
</div>
<?php get_footer(); ?>