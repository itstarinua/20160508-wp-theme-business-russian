<?php
/********************************************
*********************************************
Основной шаблон вывода подвала (footer) сайта
*********************************************
********************************************/

//Выводим два сайдбара в футаре. Один с float:left, другой с float:right ?>
		
	</div> <!-- End of #page-body-container -->
	<footer>
		<div id="footer-sidebar">
			<?php if ( function_exists('dynamic_sidebar') ) :?> 
			<ul id="sidebar-footer-left">
				<?php dynamic_sidebar('sidebar-footer-left'); ?>
			</ul>
			<ul id="sidebar-footer-right">
				<?php dynamic_sidebar('sidebar-footer-right'); ?>
			</ul>
			<?php endif; ?>
		</div>
	<?php //Выводим копирайт в футаре. ?>	
		<div id="footer-copyright" class="row text-center">
			2016 <i class="fa fa-copyright"></i> International Education Centre Business Russian IC<br>
			Website is developed by <a title="Web-master DStaroselskyi" href="https://plus.google.com/u/0/110295925295050770002/posts" rel="nofollow">DStaroselskyi</a>
		</div>
	</footer>
	<!--div class="subscribe-form-popup">
		<div class="subscribe-form-popup-container">
			<div class="subscribe-form-popup-fone">
				<i class="fa fa-times" aria-hidden="true"></i>
			</div>
			<div class="subscribe-form">
				<?php get_getresponse_form(); ?>
				<p  class="subscribe-footer"><?php _e('Мы против спама. Ваши данные никогда не будут переданы 3-м лицам.','dms-business-russian');?></p>
			</div>
		</div>
	</div-->
	<div class="subscribe-form-popup form-popup-type-1 PopUp-form-show-<?php echo\DStaroselskiy\Theme\Business_Russian\GET_RESPONSE_FORM::$subscribe_form_popup_show;?> <?php echo \DStaroselskiy\Theme\Business_Russian\GET_RESPONSE_FORM::get_class_subscribe_form_popup();?>">

		<div class="subscribe-form-popup-container">
			<div class="subscribe-form-popup-fone">
				<i class="fa fa-times" aria-hidden="true"></i>
			</div>
			<div class="subscribe-form">
                            <h4 class="theme-page-h2">Get news & free content</h4>
				<?php get_getresponse_form(); ?>
				<p  class="subscribe-footer text-center"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <?php _e('Zero spam. Unsubsribbe any time.','dms-business-russian');?></p>
			</div>
		</div>
	</div>
<?php
	wp_footer(); // Шункция вывода скриптов ядра вордпресса
?>
</body>
</html>