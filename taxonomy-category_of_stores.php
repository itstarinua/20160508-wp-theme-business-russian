<?php
/********************************************
*********************************************
Description: Основной шаблон вывода записей 
произвольного типа "Библиотеки" относящихся 
к таксономии категории историй 
Author: DStaroselskiy (it.star.varvar@gmail.com)
Author URI: https://plus.google.com/u/0/110295925295050770002/posts
Version: 0.1
Date: 12/06/2016
*********************************************
********************************************/

get_header(); 
$current_term = get_queried_object();
?>
<div class="category-title-container">
	<img src="<?php echo get_stylesheet_directory_uri();?>/images/category-title.jpg">
	<div class="category-title-cont">
		<div class="theme-container text-left">
			<h1 class="single-title"><?php echo ( isset($current_term->label) ? $current_term->label : $current_term->name); ?></h1>
			<div class="category-description"><?php echo category_description(); ?></div>
		</div>
	</div>
</div>
<div id="page-body">
	
	<div class="cat-info">
		<h5 class="widgettitle"><?php _e('Категории','dms-business-russian');?></h5>
		<?php $categorys = get_terms( 'category_of_stores', array(
			'taxonomy'      => 'post',
			'orderby'       => 'name', 
			'order'         => 'ASC',
			'hide_empty'    => false, 
			'fields'        => 'all', 
			'hierarchical'  => false, 
			'get'           => 'all',
			'pad_counts'    => false, 
			'update_term_meta_cache' => true,
		) ); ?>
		<ul class="terms-lists">
			<?php foreach ($categorys as $category) { 
				$term_select = "";
				if( $current_term->term_id == $category->term_id ) $term_select = "term-active"; ?>
				<li class="terms term_id-<?php echo  $category->term_id;?> term_slug-<?php echo  $category->slug;?> <?php echo $term_select ;?>"><a href="<?php echo get_term_link( $category->term_id, 'category_of_books' );?>" title="<?php echo $category->name;?>"><?php echo $category->name;?></a></li>
			<?php } ?>
		</ul>	
	</div>
	<div id="left-container">
		<div class="category-content">
			<?php if ( have_posts() ) {
				while ( have_posts() ) { 
					the_post(); 
					$the_ID = get_the_ID();?>
					<a class="post-info" href="<?php the_permalink(); ?>" title="<?php echo sprintf(__('Читать далее %s','dms-business-russian'),the_title())?>">
						<div class="category-img-content">
							<?php if( has_post_thumbnail( $the_ID ) ) {
								echo get_the_post_thumbnail( $the_ID , 'categiry-small', array( 'class'=>'attachment-category', 'alt'=> '', 'title'=> '', ) );
							}else{
								?><div class="attachment-category wp-post-image no-photo-279x181"></div><?php						
							} ?>
						</div>
						<?php $POSTS_VIEWS = (int)get_post_meta( $the_ID, '_POSTS_VIEWS', true);
						if( empty($POSTS_VIEWS) ) $POSTS_VIEWS = 0; ?>
						<div class="post-views"><?php echo $POSTS_VIEWS; ?></div>
						<?php $post_term_list = wp_get_post_terms($the_ID, 'category_of_stores', array("fields" => "all")); ?>
						<ul class="post-category-lists">
							<?php if( count($post_term_list) > 0 ) {
								foreach ($post_term_list as $post_term) {
									?><li class="terms term_id-<?php echo  $post_term->term_id;?> term_slug-<?php echo  $post_term->slug;?>"><?php echo $post_term->name;?></li><?php
								}
							} ?>				
						</ul>	
						<h3 class="post-title"><?php the_title();?></h3>
						<div class="post-excerpt"><?php the_excerpt();?></div>
						<div class="post-link"><span><?php _e('Далее','dms-business-russian') ?> <i class="typcn typcn-arrow-right"></i></span></div>
					</a>		
				<?php } ?>
			<?php }else{
				get_template_part( 'content','none' );
			} ?>
		</div>		
		<?php if ( function_exists( 'pgntn_display_pagination' ) ) pgntn_display_pagination( 'posts' ); ?>		
	</div>
	<ul id="sidebar-right">
		<?php if ( is_active_sidebar( 'sidebar-content-right' ) ) { dynamic_sidebar( 'sidebar-content-right' ); } ?>
	</ul>
	<div style="clear:both;"></div>
   <?php if ( is_active_sidebar( 'sidebar-category-footer' ) ) {  ?>
		<ul class="category-footer-sidebar-content">
			<?php dynamic_sidebar( 'sidebar-category-footer' ); ?>
		</ul>
	<?php } ?>
</div>		
<?php 
get_footer(); 
?>