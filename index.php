<?php

/********************************************

*********************************************

Description: Основной шаблон вывода данных

Author: DStaroselskiy (it.star.varvar@gmail.com)

Author URI: https://plus.google.com/u/0/110295925295050770002/posts

Version: 0.1

Date: 22/05/2016

*********************************************

********************************************/



get_header(); 



$page_header_bg_style = "";

echo  '<div id="page-body">';

if ( have_posts() ) {

	while ( have_posts() ) {

		if( is_page() && has_post_thumbnail() ) {

			
			$page_header_bg_style = "background-image: url(".get_the_post_thumbnail_url( get_the_ID(),'full').');'. PHP_EOL;

		}

		the_post();

		// get_template_part( 'content', get_post_format() );

		the_content();	

	}

}else{

	get_template_part( 'content','404' );

}

echo '</div>';

if( !empty( $page_header_bg_style ) ) {

	echo <<<EOF

	<style>

		#page-body-container {

			$page_header_bg_style

		}

	</style>

EOF;

}

get_footer(); 



?>