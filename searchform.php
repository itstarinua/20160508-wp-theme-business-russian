<form role="search" method="get" class="search-form" action="<?php  echo esc_url( home_url( '/' ) );?>">
	<label>
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'dms-business-russian' );?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder', 'dms-business-russian' );?> " value="<?php echo get_search_query();?>" name="s" />
	</label>
	<button type="submit" class="search-submit"><?php echo esc_attr_x( 'Search', 'submit button', 'dms-business-russian' );?></button>
</form>