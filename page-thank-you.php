<?php
/********************************************
*********************************************
* Template Name: Thank-you-page

Description: Основной шаблон вывода страницы благодарности
Author: DStaroselskiy (it.star.varvar@gmail.com)
Author URI: https://plus.google.com/u/0/110295925295050770002/posts
Version: 0.1
Date: 23/07/2016
*********************************************
********************************************/

get_header(); 

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		if( is_page() && has_post_thumbnail() ) {
			echo  '<div id="page-thumbnail-title-container" class="thank-you-page-title">';
			the_post_thumbnail( 'full', array( 'class' => "post-header-thumbnail attachment" ) );
			$THUMBNAIL_TITLE = get_post_meta( get_the_ID(), '_THUMBNAIL_TITLE', true);	
			echo  '<div class="pages-thumbnail-title theme-container text-',( isset($THUMBNAIL_TITLE['orientation']) ? $THUMBNAIL_TITLE['orientation'] : 'center'),'">';
				echo '<img src="',get_stylesheet_directory_uri(),'/images/grateful_bg.png" class="page-img-title">';
				if( isset($THUMBNAIL_TITLE['title']) ) echo '<h1 class="page-thumbnail-title">'.$THUMBNAIL_TITLE['title'].'</h1>';
				if( isset($THUMBNAIL_TITLE['description']) ) echo '<h2 class="page-thumbnail-title">'.$THUMBNAIL_TITLE['description'].'</h2>';
			echo  '</div>'.PHP_EOL;
			echo  '</div>';
		}
		// if( function_exists( 'psr_show_voting_stars' ) ) psr_show_voting_stars();
		echo  '<div id="page-body" class="full-width-body">';
		the_content();	
		echo '</div> <!-- End of #page-body -->';
	}
}else{
	echo  '<div id="page-body" class="full-width-body">';
	get_template_part( 'content','404' );
	echo '</div> <!-- End of #page-body -->';
}

get_footer(); 

?>