<!DOCTYPE html>

<html <?php language_attributes();?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width">
		<meta content="L.Kotane & E.Dziuba" name="author" />
		<meta content="noindex,nofollow" name="Robots"/>
		<link rel="icon" href="<?php echo get_template_directory_uri();?>/images/favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/images/favicon.ico" type="image/x-icon" />
		 <!--[if lt IE 9]>
		 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		 <![endif]-->
		<?php
			wp_head(); 
		?>
	</head>
	<body <?php body_class('menu-black'); ?>>
		<header class="title-main">
			<div class="theme-container">
				<?php if( !(is_home() || is_front_page() ) ){ 
					$logo_url = get_theme_mod('menu-logotype-light', '');
					if( !empty($logo_url) ) { ?>
						<a class="logo-type-link" href="<?php echo home_url();?>" rel="nofollow" title="<?php _e('Go to home page','dms-business-russian');?>"><img class="site-logotype" src="<?php echo $logo_url;?>" title="<?php bloginfo('name');?>" alt="Site logotype"/></a>
					<?php }
				} ?>
				<div class="mobile-menu">
						<button type="buttom"class="" title=""><?php _e('Меню','dms-business-russian');?> <i class="fa fa-bars" aria-hidden="true"></i></button>
				</div>				
				<?php wp_nav_menu( array(
					'theme_location'  => 'primary',
					'container'       => false, 
					'menu_class'      => '', 
					'menu_id'         => '',
					'echo'            => true,
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				) ); ?>
			</div>
		</header>
		<?php if( is_home() || is_front_page() ) { ?>
		<div id="home-site-title-screen" style="background-image:url(<?php header_image();?>);">
				<?php the_custom_logo(); ?>
				<h1 style="color:<?php echo get_theme_mod( 'site-title-color','#FFFFFF' );?>"><?php echo bloginfo('name');?></h1>
				<h2 style="color:<?php echo get_theme_mod( 'site-description-color','#A7A7A7' );?>"><?php echo bloginfo('description');?></h2>
		</div>
		<?php } ?>
		<div id="page-body-container">