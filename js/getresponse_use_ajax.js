/*
Описание обработчика отправки данных формы захвата для GetResponse
*/

(function($){
	jQuery(document).ready(function(){
		jQuery('.getresponse-form').on('submit',function(){
			window.ga('send', 'event', 'SubscribeForm', 'SendData');
			//_gaq.push(['_trackEvent', 'SubscribeForm', 'SendData']);
			var $send_form = jQuery(this);
			var $send_data = $send_form.serialize();
			jQuery.ajax({ 
				type: 'POST',
				url:  '?action=json',
				dataType: 'json',
				data: $send_data,
				beforeSend: function(){
					$send_form.find('.submit-getresponse-form').prop('disabled', true);
					$send_form.find('.getresponse-form-report').empty();
				},
				error: function(){
					$send_form.find('.getresponse-form-report').append('<p class="report-point error">ERROR: Server not respons</p>');
					if( typeof grecaptcha != 'undefined')  grecaptcha.reset();
				},
				success: function( resive_data ){
					console.log(resive_data);
					if( resive_data.status == 'redirect' ) {
						//_gaq.push(['_trackEvent', 'SubscribeForm', 'SendData']);
						window.ga('send', 'event', 'SubscribeForm', 'Registrated');
						window.location.assign( resive_data.report );
						return true;

					};					
					jQuery.each(resive_data.report, function(index, value){						
						$send_form.find('.getresponse-form-report').append('<p class="report-point '+value.code+'">'+value.msg+'</p>');
						//_gaq.push(['_trackEvent', 'SubscribeForm', 'Errors']);
						window.ga('send', 'event', 'SubscribeForm', 'Errors');
					});
					if( ( resive_data.status == 'error' ) && ( typeof grecaptcha != 'undefined') ) grecaptcha.reset();
				},
				complete: function(){
					$send_form.find('.submit-getresponse-form').prop('disabled', false);
					if( typeof grecaptcha != 'undefined')  grecaptcha.reset();
				}			
			});
			return false;
		});
	});
}(jQuery))