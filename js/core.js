/**********************************************
***********************************************
Основной шаблон скриптов для сайта
***********************************************
**********************************************/

function WINDOW_SIZE(){
	this.window_h = jQuery(window).outerHeight();
	this.document_h = jQuery(document).outerHeight();
	this.html = jQuery('html');
	this.body = jQuery('body');
	this.header = jQuery('header');
	this.footer = jQuery('footer');
	this.header_offset_top = this.header.offset().top;
	//this.header.css("top", this.header_offset_top);
	window.WINDOW_SIZE = this;
	this.calc = function(){
		jQuery('#page-body-container').css('min-height','auto' );
		header_h = window.WINDOW_SIZE.header.outerHeight(true);
		footer_h = window.WINDOW_SIZE.footer.outerHeight(true);
		if( this.html_h < this.document_h ) {
			body_h = window.WINDOW_SIZE.document_h - footer_h - header_h - window.WINDOW_SIZE.header_offset_top;		
			jQuery('#page-body-container').css('min-height',body_h );
		}		
	}
	jQuery(window).resize(function(){
		window.WINDOW_SIZE.calc();
		jQuery('body header #menu-verhnee').removeClass('menu-show');
	});
	jQuery('html, body').scroll(function(){
		jQuery('body header #menu-verhnee').removeClass('menu-show');
	});
	jQuery(window).scroll(function(){
		jQuery('body header #menu-verhnee').removeClass('menu-show');
	});
	jQuery('img').load(function(){
		window.WINDOW_SIZE.calc();
	});
	
	this.calc();	
}
/************************************************
Выравния списка категорий по ширине
************************************************/
function widget_term_width( element ){
	$obj = jQuery( element );
	$elements = $obj.find('.terms');
	$elements.find('a').width('auto');
	if( $elements.length == 0) return false;
	$left_position = $obj.offset().left;
	$cont_width = $obj.width();
	$width_50 = $cont_width / 2;
	$top = $elements.eq(0).offset().top;
	$elem_count = 1;
	for($i=0;$i<$elements.length;$i++){
		if( $elements.eq($i).width() > $width_50 ) {
			$elements.eq($i).width( '100%' );
			$elem_count = 1;
		}else{
			if( $elements.eq($i+1).length == 0) {
				$elem_top = $top + $elements.eq($i).height();
			}else{
				$elem_top =  $elements.eq($i+1).offset().top;
			}
			if( $top != $elem_top ) {
				$right_position = $elements.eq($i).offset().left + $elements.eq($i).width();
				$elem_padding = parseInt( ( $cont_width - ( $right_position - $left_position ) ) / $elem_count );
				for ($j = $elem_count; $j > 0; $j--) {
					$a = $elements.eq($i-$j+1).children();
					$a_width = $a.width();
					$a_width = $a.width( $a_width + $elem_padding);
				}
				$top = $elem_top;
				$elem_count = 1;
			}else{
				$elem_count++;
			}
		}
	}
};
function init_widget_term_width(){
	$terms_lists = jQuery('.terms-lists');
	if( $terms_lists.length == 0) return false;
	$terms_lists.each(function(index, element){
		widget_term_width( element );
	});
}

jQuery(document).ready(function(){
	new WINDOW_SIZE();
	init_widget_term_width();
	
	jQuery(window).resize(init_widget_term_width);
	
	jQuery('.scroll-to > a').on('click',function(e){
		e.preventDefault();
		block_id = jQuery(this).attr('href');
		var block_top = jQuery(block_id).offset();		
		if( block_top == 'undefined' ) return false;				
		if(jQuery.browser.safari || jQuery.browser.chrome){
			jQuery('body').animate( { scrollTop: block_top.top }, 1100 );			
		}else{
			jQuery('html').animate( { scrollTop: block_top.top }, 1100 );
		}		
		return false;
	});
	jQuery('.mobile-menu > button').on('click',function(){
		jQuery('body header #menu-verhnee').toggleClass('menu-show');
	})
	
	jQuery('[href="#show-subscribe-form"]').on('click',function(e){
		e.preventDefault();
		window.ga('send', 'event', 'SubscribeForm', 'Open');
		console.log(ga);
		//_gaq.push(['_trackEvent', 'SubscribeForm', 'Open']);
		if( !jQuery('html').hasClass('show-subscribe-form') ) jQuery('html').addClass('show-subscribe-form');
	});
	jQuery('.subscribe-form-popup-fone').on('click',function(){
		window.ga('send', 'event', 'SubscribeForm', 'Close');
		//_gaq.push(['_trackEvent', 'SubscribeForm', 'Close']);
		jQuery('html').removeClass('show-subscribe-form');
	});
	/*Обработка запуска поп-ап окна подписки на рассылку*/
	 if( jQuery('.subscribe-form-popup.start-on-load').length > 0 ) {
			window.ga('send', 'event', 'SubscribeForm', 'AutoStart');
			//_gaq.push(['_trackEvent', 'SubscribeForm', 'AutoStart']);
	        setTimeout("jQuery('html').addClass('show-subscribe-form')", 15000);// 6000);
	    } 
	/*Обработка меню*/
	if( jQuery('body').hasClass('tax-category_of_stores') || jQuery('body').hasClass('single-library') ) {						
		$elem = jQuery('header a[href$="library/"]').parent();
		if( !$elem.hasClass('current-menu-item')) $elem.addClass('current-menu-item');
	}else if( jQuery('body').hasClass('tax-category_of_books') || jQuery('body').hasClass('single-books') ){
		$elem = jQuery('header a[href$="store/"]').parent();
		if( !$elem.hasClass('current-menu-item')) $elem.addClass('current-menu-item');
	}else if( jQuery('body').hasClass('category') || jQuery('body').hasClass('single-post') ){
		$elem = jQuery('header a[href$="blog/"]').parent();
		if( !$elem.hasClass('current-menu-item')) $elem.addClass('current-menu-item');
	}

	
}); 