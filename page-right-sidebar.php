<?php
/********************************************
*********************************************
* Template Name: Right SideBar

Description: Основной шаблон вывода данных
Author: DStaroselskiy (it.star.varvar@gmail.com)
Author URI: https://plus.google.com/u/0/110295925295050770002/posts
Version: 0.1
Date: 21/10/2016
*********************************************
********************************************/

get_header(); 

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		if( is_page() && has_post_thumbnail() ) {
			echo  '<div id="page-thumbnail-title-container">';
			the_post_thumbnail( 'full', array( 'class' => "post-header-thumbnail attachment" ) );
			$THUMBNAIL_TITLE = get_post_meta( get_the_ID(), '_THUMBNAIL_TITLE', true);	
			echo  '<div class="pages-thumbnail-title theme-container text-',( isset($THUMBNAIL_TITLE['orientation']) ? $THUMBNAIL_TITLE['orientation'] : 'center'),'">';						
				if( isset($THUMBNAIL_TITLE['title']) ) echo '<h1 class="page-thumbnail-title">'.$THUMBNAIL_TITLE['title'].'</h1>';
				if( isset($THUMBNAIL_TITLE['description']) ) echo '<h2 class="page-thumbnail-title">'.$THUMBNAIL_TITLE['description'].'</h2>';
			echo  '</div>'.PHP_EOL;
			echo  '</div>';
		}
		// if( function_exists( 'psr_show_voting_stars' ) ) psr_show_voting_stars();
		//echo  '<div id="page-body" class="full-width-body">';
		?>
		<div id="page-body"> 
			<div id="left-container">	
				<?php the_content(); ?>

			</div>
			<ul id="sidebar-right">
				<?php if ( is_active_sidebar( 'sidebar-content-right' ) ) { dynamic_sidebar( 'sidebar-content-right' ); } ?>
			</ul>
			<div style="clear:both;"></div>	
		</div><?php 

		// echo '</div> <!-- End of #page-body -->';
	}
}else{
	echo  '<div id="page-body" class="full-width-body">';
	get_template_part( 'content','404' );
	echo '</div> <!-- End of #page-body -->';
}

get_footer(); 

?>