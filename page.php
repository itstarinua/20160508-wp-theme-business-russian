<?php
/********************************************
*********************************************
Description: Основной шаблон вывода данных
Author: DStaroselskiy (it.star.varvar@gmail.com)
Author URI: https://plus.google.com/u/0/110295925295050770002/posts
Version: 0.1
Date: 22/05/2016
*********************************************
********************************************/

get_header(); 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		if( is_page() && has_post_thumbnail() ) {
			echo  '<div id="page-thumbnail-title-container">';
			the_post_thumbnail( 'full', array( 'class' => "post-header-thumbnail attachment" ) );
			$THUMBNAIL_TITLE = get_post_meta( get_the_ID(), '_THUMBNAIL_TITLE', true);	
			echo  '<div class="pages-thumbnail-title theme-container text-',( isset($THUMBNAIL_TITLE['orientation']) ? $THUMBNAIL_TITLE['orientation'] : 'center'),'">';										$THUMBNAIL_TITLE = get_post_meta( get_the_ID(), '_THUMBNAIL_TITLE', true);			
				if( isset($THUMBNAIL_TITLE['title']) ) echo '<h1 class="page-thumbnail-title">'.$THUMBNAIL_TITLE['title'].'</h1>';
				if( isset($THUMBNAIL_TITLE['description']) ) echo '<h2 class="page-thumbnail-title">'.$THUMBNAIL_TITLE['description'].'</h2>';
			echo  '</div>';
			$page_header_bg_style = "";
			echo  '</div>';
		}
		echo  '<div id="page-body">';
		// if( function_exists( 'psr_show_voting_stars' ) ) psr_show_voting_stars();		
		the_content();	
		echo '</div> <!-- End of #page-body -->';
	}
}else{
	echo  '<div id="page-body">';
	get_template_part( 'content','404' );
	echo '</div> <!-- End of #page-body -->';
}
get_footer(); 

?>