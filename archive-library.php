<?php
/********************************************
*********************************************
Description: Основной шаблон вывода данных кастомного типа записи Store (Истории)
Author: DStaroselskiy (it.star.varvar@gmail.com)
Author URI: https://plus.google.com/u/0/110295925295050770002/posts
Version: 0.1
Date: 12/06/2016
*********************************************
********************************************/

get_template_part( 'taxonomy','category_of_stores' );

?>