<?php
/*********************************************
Описание основного класса управления темой
by DStaroselskiy 

Version: 0.1
Date: 2016-05-03
*********************************************/
namespace DStaroselskiy\Theme\Business_Russian{
	if( !class_exists('\GetResponse') ) { require_once( DMS_THEME_DIR . '/include/GetResponseAPI.class.php'); }

	class GET_RESPONSE_FORM {
		protected $report = array();
		protected $form_date = array();
		protected $is_error = false;

		public function customize_register( $wp_customize ) {
			// Добавляем секцию ввода данных getresponse
			$wp_customize->add_section(
				'getresponse_settings', // id секции
				array(
					'title'     => 'API GetResponse',
					'priority'  => 201 // Приоритет сделаем чуть побольше, благодаря этому секция окажется в самом низу кастомайзера
				)
			);
			// Полее ввода Доступа
			$wp_customize->add_setting(
				'getresponse_api_key', // id
				array(
					'capability'         => 'edit_theme_options',
					'type'		         => 'theme_mod',
				)
			);
			$wp_customize->add_control(
				'getresponse_api_key', // id
				array(
					'section'  => 'getresponse_settings', // id секции
					'label'    => 'API key',
					'type'     => 'text' // текстовое поле
				)
			);
			// Полее Название Компании
			$wp_customize->add_setting(
				'getresponse_campaign_name', // id
				array(
					'capability'         => 'edit_theme_options',
					'type'		         => 'theme_mod',
				)
			);
			$wp_customize->add_control(
				'getresponse_campaign_name', // id
				array(
					'section'  => 'getresponse_settings', // id секции
					'label'    => 'Сampaign name',
					'type'     => 'text' // текстовое поле
				)
			);
			//Настройка подключения AJAX для обработки отправки данных с формы
			$wp_customize->add_setting(
				'getresponse_use_ajax', 
				array(
					'default'    =>  'true',
				)
			);
			$wp_customize->add_control(
				'getresponse_use_ajax', // id
				array(
					'section'   => 'getresponse_settings', 
					'label'     => __('Использовать AJAX?','dms-business-russian'), 
					'type'      => 'radio',
					'choices' => array(
			            'true' => __('Да','dms-business-russian'),
			            'false' => __('Нет','dms-business-russian'),			            
			        ), 
				)
			);
			//Настройка подключения reCAPTCHA
			$wp_customize->add_setting(
				'getresponse_use_reCAPTCHA', 
				array(
					'default'    =>  'false',
				)
			);
			$wp_customize->add_control(
				'getresponse_use_reCAPTCHA', // id
				array(
					'section'   => 'getresponse_settings', 
					'label'     => __('Включить reCAPTCHA?','dms-business-russian'), 
					'type'      => 'radio',
					'choices' => array(
			            'true' => __('Да','dms-business-russian'),
			            'false' => __('Нет','dms-business-russian'),			            
			        ), 
				)
			);
			// Полее ввода ключа reCAPTCHA
			$wp_customize->add_setting(
				'getresponse_reCAPTCHA_key', // id
				array(
					'capability'         => 'edit_theme_options',
					'type'		         => 'theme_mod',
				)
			);
			$wp_customize->add_control(
				'getresponse_reCAPTCHA_key', // id
				array(
					'section'  => 'getresponse_settings', // id секции
					'label'    => __('reCAPTCHA public key','dms-business-russian'), 
					'type'     => 'text' // текстовое поле
				)
			);
			// Полее ввода секретного ключа reCAPTCHA
			$wp_customize->add_setting(
				'getresponse_reCAPTCHA_privet_key', // id
				array(
					'capability'         => 'edit_theme_options',
					'type'		         => 'theme_mod',
				)
			);
			$wp_customize->add_control(
				'getresponse_reCAPTCHA_privet_key', // id
				array(
					'section'  => 'getresponse_settings', // id секции
					'label'    => __('reCAPTCHA privet key','dms-business-russian'), 
					'type'     => 'text' // текстовое поле
				)
			);
			// Полее ввода ссылки переадресации пользователя при удачной отправки данных
			$wp_customize->add_setting(
				'getresponse_redirect_to', // id
				array(
					'capability'         => 'edit_theme_options',
					'type'		         => 'theme_mod',
				)
			);
			$wp_customize->add_control(
				'getresponse_redirect_to', // id
				array(
					'section'  => 'getresponse_settings', // id секции
					'label'    => __('Ссылка переадресации пользователя при удачной отправки данных.','dms-business-russian'), 
					'type'     => 'url' // текстовое поле
				)
			);
		}
		public function get_getresponse_form( $echo = true ){
			$name = __('Name','dms-business-russian');
			$name_placeholder = __('Put your name here...','dms-business-russian');
			$email = __('Email','dms-business-russian');
			$email_placeholder = __('Put your email here...','dms-business-russian');
			$btn_text = __('Subscribe','dms-business-russian');
			$nonce = wp_nonce_field( 'getresponse', 'getresponse-nonce', true, false );
			$val_name = ( isset($this->form_date['getresponse-name']) ? $this->form_date['getresponse-name'] : '');
			$val_email = ( isset($this->form_date['getresponse-email']) ? $this->form_date['getresponse-email'] : '');
			$report = "";
			if( count($this->report) > 0 ) foreach( $this->report as $report_date) {
				$report .= '<p class="report-point '.$report_date['status'].'">'.$report_date['msg'].'</p>'. PHP_EOL;
			}
			$use_reCAPTCHA = '';
			$getresponse_reCAPTCHA_key = get_theme_mod( 'getresponse_reCAPTCHA_key', '' );
			$getresponse_reCAPTCHA_privet_key = get_theme_mod( 'getresponse_reCAPTCHA_privet_key', '' );
			if( ( get_theme_mod( 'getresponse_use_reCAPTCHA', 'false' ) == 'true' )
				&& !empty( $getresponse_reCAPTCHA_key  )
				&& !empty( $getresponse_reCAPTCHA_privet_key  )
			) {
				$use_reCAPTCHA = '<div class="g-recaptcha" data-sitekey="'.get_theme_mod( 'getresponse_reCAPTCHA_key', '' ).'"></div>';
				if( !wp_script_is( 'getresponse_reCAPTCHA', 'registered ') ) wp_enqueue_script( 'getresponse_reCAPTCHA', 'https://www.google.com/recaptcha/api.js',array(),null,true);
			}
			$form = <<<EOF
				<form class="getresponse-form" method="POST">
					<div class="left-part1">
						<table class="contact-form-table">
							<tr>
								<td>$name <span>*</span></td>
								<td><input name="getresponse-name" size="40" class="input-name" placeholder="{$name_placeholder}" type="text" value="{$val_name}"></td>
							</tr>
							<tr>
								<td>$email <span>*</span></td>
								<td><input name="getresponse-email" size="40" class="input-email" placeholder="{$email_placeholder}" type="text" value="{$val_email}"></td>
							</tr>
						</table>
					</div>
					<div class="getresponse-reCAPTCHA">
						{$use_reCAPTCHA}
					</div>
					<div class="right-part1 text-center">
						<div>
							<button class="submit-getresponse-form" type="submit">$btn_text</button>
						</div>
					</div>					
					<div class="getresponse-form-report">
						{$report}
					</div>
					$nonce
				</form>
EOF;
			if( $echo == true ) echo $form;
			if( get_theme_mod( 'getresponse_use_ajax', 'false' ) == 'true' ) {
				if( !wp_script_is( 'getresponse_use_ajax', 'registered ') ) wp_enqueue_script( 'getresponse_use_ajax', DMS_THEME_URL.'/js/getresponse_use_ajax.js',array('jquery'),null,true);
			}

			return $form;
		}
		public function add_contact_to_getresponse(){
			if( !isset($_POST['getresponse-nonce']) ) return false;
			if( empty($_POST['getresponse-nonce'])
				|| !wp_verify_nonce( $_POST['getresponse-nonce'], 'getresponse' )
			) {
				$this->is_error = true;
				$this->report[] = array(
					'code' => 'error',
					'msg'  => __('Секретный ключь не верен. Пожалуйста перезагрузите страницу и попробуйте еще раз.','dms-business-russian'),
				);			
			}
			//Проверка Капчи

			$getresponse_reCAPTCHA_key = get_theme_mod( 'getresponse_reCAPTCHA_key', '' );
			$getresponse_reCAPTCHA_privet_key = get_theme_mod( 'getresponse_reCAPTCHA_privet_key', '' );

			if( ( get_theme_mod( 'getresponse_use_reCAPTCHA', 'false' ) == 'true' )
				&& !empty( $getresponse_reCAPTCHA_key )
				&& !empty( $getresponse_reCAPTCHA_privet_key )
			) {
				if( isset( $_POST['g-recaptcha-response'] ) && !empty($_POST['g-recaptcha-response']) ) {
				    $google_url="https://www.google.com/recaptcha/api/siteverify";
				    $secret = get_theme_mod( 'getresponse_reCAPTCHA_privet_key', '' );
				    $ip=$_SERVER['REMOTE_ADDR'];
				    $url=$google_url."?secret=".$secret."&response=".$_POST['g-recaptcha-response']."&remoteip=".$ip;
				    
				    $curl = curl_init();
				    curl_setopt($curl, CURLOPT_URL, $url);
				    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
				    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
				    $curlData = curl_exec($curl);
				    curl_close($curl); 
				    
				    $res= json_decode($curlData, true);
				    //reCaptcha введена
				    if( $res['success'] ) {
				            // Продолжаем проверку данных формы
				    }else{
				        $this->is_error = true;
						$this->report[] = array(
							'code' => 'error',
							'msg'  => __('Пожалуйста заполните капчу.','dms-business-russian'),
						);
				    }
				}else{
				    $this->is_error = true;
					$this->report[] = array(
						'code' => 'error',
						'msg'  => __('Пожалуйста заполните капчу.','dms-business-russian'),
					);
				}
				
			}

			if( isset( $_POST['getresponse-name'] ) ) {
				if( empty(  $_POST['getresponse-name']  ) ) {
					$this->is_error = true;
					$this->report[] = array(
						'code' => 'error',
						'msg'  => __('Пожалуйста введите ваше ИМЯ.','dms-business-russian'),
					);
				}else{
					$this->form_date['getresponse-name'] = esc_attr($_POST['getresponse-name'] );
				}
			}
			if( isset( $_POST['getresponse-email'] ) ) {
				if( !is_email(  $_POST['getresponse-email']  ) ) {
					$this->is_error = true;
					$this->report[] = array(
						'code' => 'error',
						'msg'  => __('Пожалуйста введите корректный email.','dms-business-russian'),
					);
				}
				$this->form_date['getresponse-email'] = esc_attr($_POST['getresponse-email'] );				
			}else{
				$this->is_error = true;
				$this->report[] = array(
					'code' => 'error',
					'msg'  => __('Пожалуйста введите email.','dms-business-russian'),
				);
			}
			if( !$this->is_error ) {
				$this->send_data_to_getresponse();
			}
			if( isset($_GET['action']) && ( $_GET['action'] == 'json') ){
				status_header( 200 );
				header('Content-Type: application/json');
				$redirect_url = get_theme_mod( 'getresponse_redirect_to', '' );
				if( !empty( $redirect_url ) && ( !$this->is_error )  ) {
					die(
						json_encode( 
							array(
								'status' => 'redirect',
								'report' => $redirect_url,
							)
						)
					);

				}else{
					die(
						json_encode( 
							array(
								'status' => ( $this->is_error ? 'error' : 'ok'),
								'report' => $this->report,
							)
						)
					);
				}
			}
		}

		public function send_data_to_getresponse(){
			$API_key = get_theme_mod( 'getresponse_api_key', '' );
			$campaign = get_theme_mod( 'getresponse_campaign_name', '' );
			if( empty( $API_key ) ) {
				$this->is_error = true;
				$this->report[] = array(
					'code' => 'error',
					'msg'  => __('Пожалуйста укажите API-key для GetResponse.','dms-business-russian'),
				);
			}
			if( empty( $campaign ) ) {
				$this->is_error = true;
				$this->report[] = array(
					'code' => 'error',
					'msg'  => __('Пожалуйста укажите имя campaign для GetResponse.','dms-business-russian'),
				);
			}

			if( $this->is_error ) return false;

			$api = new \GetResponse($API_key);

			// Connection Testing
			if( $api->ping() != "pong") {
				$this->is_error = true;
				$this->report[] = array(
					'code' => 'error',
					'msg'  => __('Ошибка связи с серверм GetResponse.','dms-business-russian'),
				);
				return false;
			}
			$campaigns = (array)$api->getCampaigns( 'EQUALS',  $campaign );
			if( count( $campaigns ) == 0 ) {
				$this->is_error = true;
				$this->report[] = array(
					'code' => 'error',
					'msg'  => __('Пожалуйста укажите правильное имя campaign для GetResponse.','dms-business-russian'),
				);
				return false;
			}
			$campaignIDs = array_keys($campaigns);
			$contacts 	= $api->addContact($campaignIDs[0], ( ( isset($this->form_date['getresponse-name']) && !empty($this->form_date['getresponse-name']) )? $this->form_date['getresponse-name'] : 'No-Name'),  $this->form_date['getresponse-email'] );
			if( $contacts->queued == true) {
				$this->report[] = array(
					'code' => 'ok',
					'msg'  => __('Подписка успешно оформлена.','dms-business-russian'),
				);
			}elseif( $contacts->code == -1) {
				$this->is_error = true;
				$this->report[] = array(
					'code' => 'error',
					'msg'  => $contacts->message,
				);
			}else{
				$this->is_error = true;
				$this->report[] = array(
					'code' => 'error',
					'msg'  => __('Вы уже подписаны на рассылку.','dms-business-russian'),
				);
			}
		}
		/*Дополнение обработка данных для запуска поп-ап формы для нового гостя*/
		public static  $subscribe_form_popup = "";
        public static  $subscribe_form_popup_show = 0;
        public static function get_class_subscribe_form_popup(){
            return self::$subscribe_form_popup;  
        }
		function __construct(){                     
                        if( !( isset($_COOKIE['subscribe-form-popup']) && ( (int)$_COOKIE['subscribe-form-popup'] > 2 ) ) 
                            && !isset( $_COOKIE['subscribe-form-popup-is-show'] )
                       	) { 
                            self::$subscribe_form_popup = "start-on-load";
                            self::$subscribe_form_popup_show = ( isset($_COOKIE['subscribe-form-popup']) ? (int)$_COOKIE['subscribe-form-popup'] + 1 : self::$subscribe_form_popup_show );
                            \setcookie("subscribe-form-popup", self::$subscribe_form_popup_show, 4635843656,  '/');
                            \setcookie("subscribe-form-popup-is-show", "true", 0,  '/');
                        } 
			add_action( 'customize_register', array( &$this, 'customize_register') ,12,1 );
			add_action( 'init', array( &$this, 'add_contact_to_getresponse') ,12,0 );
		} 

	}
}
namespace {
	global $GET_RESPONSE_FORM;
	$GET_RESPONSE_FORM = new \DStaroselskiy\Theme\Business_Russian\GET_RESPONSE_FORM();

	function get_getresponse_form( $type = 'full', $echo = true) {
		global $GET_RESPONSE_FORM;
		switch( $type ){
			default: $GET_RESPONSE_FORM->get_getresponse_form( $echo );
		}
	}
}

?>