<?php
/*********************************************
Описание класса работы с метабоксами 
для записей типа:
1) Книга
by DStaroselskiy 

Version: 0.1
Date: 2016-05-29
*********************************************/
namespace DStaroselskiy\Theme\Business_Russian;

class METABOXES {
	//Дефолтное состояние параметров книги
	protected $book_parameters_default = array(
		'author' => 0,
		'cost_val' => '',
		'cost_akcia_val' => '',
		'level_val' => '',
		'pages_count_val' => '0',
		'file_size_val' => '0',
		'publisher_val' => '',
		'payed_market_val' => '',
	);
	
	//Метобокс с описание книги
	public function metabox_book_show() { 
		//Подготавливаем данные для мета бокса
		global $post;
		$post_id = $post->ID;
		
		$BOOK_PARAMETERS = wp_parse_args( get_post_meta( $post_id, '_BOOK_PARAMETERS', true), $this->book_parameters_default);

		$dms_posts = new \WP_Query();
		$authors_list = $dms_posts->query( array(
			'post_type' 	=> 'our-author',
			'post_status' 	=> 'any',
			'posts_per_page' => -1,			
		)); 
		$select_author_lists = '';
		if( count($authors_list) > 0 )
			foreach( $authors_list as $author ){
				if( !empty($author->post_title) ) {
					$select_author_lists .= '<option value="'.$author->ID.'"'.( ($author->ID == $BOOK_PARAMETERS['author'])? ' selected="selected"': '' ).'>'.$author->post_title.'</option>' . PHP_EOL;
				}
			}
		unset($authors_list);
		unset($dms_posts);
		$author_text = __('Автор','dms-business-russian');
		$select_author_text = __('Выберите автора','dms-business-russian');
		$cost_text = __('Цена','dms-business-russian');
		$cost_akcia_text = __('Промо цена','dms-business-russian');
		$level_text = __('Уровень','dms-business-russian');
		$pages_count_text = __('Кол.страниц/минут','dms-business-russian');
		$file_size_text = __('Размер файла','dms-business-russian');
		$publisher_text = __('Издательство','dms-business-russian');
		$payed_market_text = __('Ссылка на магазин Amazon','dms-business-russian');
		$payed_paypal_market_text = __('ID of hosted_button_id PayPal','dms-business-russian');
		$key_for_show_link_by_content_load_text = __('Секредный код.','dms-business-russian');
		$key_for_show_link_by_content_load_text_v = __('Код при котором будет отображаться кнопка СКАЧАТЬ, добавляется параметром в обратную ссылку PayPal. Пример http://sity.com/store/book/?action=код','dms-business-russian');
		$link_for_content_load_text = __('Ссылка для скачивания материала','dms-business-russian');
		$cost_val = $BOOK_PARAMETERS['cost_val'];
		$cost_akcia_val = $BOOK_PARAMETERS['cost_akcia_val'];
		$level_val = $BOOK_PARAMETERS['level_val'];
		$pages_count_val = $BOOK_PARAMETERS['pages_count_val'];
		$file_size_val = $BOOK_PARAMETERS['file_size_val'];
		$publisher_val = $BOOK_PARAMETERS['publisher_val'];
		$payed_market_val = $BOOK_PARAMETERS['payed_market_val'];
		$hosted_button_id = ( isset($BOOK_PARAMETERS['paypal_hosted_button_id']) ? $BOOK_PARAMETERS['paypal_hosted_button_id'] : '' );
		$link_paypal_secret_action_key = ( isset($BOOK_PARAMETERS['paypal_secret_action']) ? $BOOK_PARAMETERS['paypal_secret_action'] : '' );
		$link_for_content_load = ( isset($BOOK_PARAMETERS['link_for_content_load']) ? $BOOK_PARAMETERS['link_for_content_load'] : '' );
		echo <<<EOF
			<div><lable><div>$author_text:</div>
				<select style="width:100%;" name="BOOK_PARAMETERS[AUTHOR]">
					<option value="0">$select_author_text</option>
					$select_author_lists
				<select>
			</lable></div>
			<div><lable><div>$cost_text:</div> <input style="width:100%;" type="text" name="BOOK_PARAMETERS[COST]" value="$cost_val"></lable></div>
			<div><lable><div>$cost_akcia_text:</div> <input style="width:100%;" type="text" name="BOOK_PARAMETERS[COST_AKCIA]" value="$cost_akcia_val"></lable></div>
			<div><lable><div>$level_text:</div> <input style="width:100%;" type="text" name="BOOK_PARAMETERS[LEVEL]" value="$level_val"></lable></div>
			<div><lable><div>$pages_count_text:</div> <input style="width:100%;" type="text" name="BOOK_PARAMETERS[PAGES_COUNT]" value="$pages_count_val"></lable></div>
			<div><lable><div>$file_size_text:</div> <input style="width:100%;" type="text" name="BOOK_PARAMETERS[FILE_SIZE]" value="$file_size_val"></lable></div>
			<div><lable><div>$publisher_text:</div> <input style="width:100%;" type="text" name="BOOK_PARAMETERS[PUBLISHER]" value="$publisher_val"></lable></div>
			<div><lable><div>$payed_market_text:</div> <input style="width:100%;" type="text" name="BOOK_PARAMETERS[PAYED_MARKET]" value="$payed_market_val"></lable></div>
			<div><lable><div>$payed_paypal_market_text:</div> <input style="width:100%;" type="text" name="BOOK_PARAMETERS[PAYPAL_HOSTED_BUTTON_ID]" value="$hosted_button_id"></lable></div>
			<div>
				<lable><div>{$key_for_show_link_by_content_load_text}:</div> <input style="width:100%;" type="text" name="BOOK_PARAMETERS[PAYPAL_SECRET_KEY]" value="$link_paypal_secret_action_key"></lable>
				<p>{$key_for_show_link_by_content_load_text_v}</p>
			</div>
			<div><lable><div>$link_for_content_load_text:</div> <input style="width:100%;" type="text" name="BOOK_PARAMETERS[LINK_FOR_CONTENT_LOAD]" value="$link_for_content_load"></lable></div>
EOF;
	} 
	
	//Метобокс со списком авторов для статей
	public function metabox_author_show() { 
		//Подготавливаем данные для мета бокса
		global $post;
		$post_id = $post->ID;
		
		$AUTHOR_ID = (int)get_post_meta( $post_id, '_AUTHOR_ID', true);

		if( empty($AUTHOR_ID) ) $AUTHOR_ID = 0;
		$dms_posts = new \WP_Query();
		$authors_list = $dms_posts->query( array(
			'post_type' 	=> 'our-author',
			'post_status' 	=> 'any',
			'posts_per_page' => -1,			
		)); 
		$select_author_lists = '';
		if( count($authors_list) > 0 )
			foreach( $authors_list as $author ){
				if( !empty($author->post_title) ) {
					$select_author_lists .= '<option value="'.$author->ID.'"'.( ($author->ID == $AUTHOR_ID)? ' selected="selected"': '' ).'>'.$author->post_title.'</option>' . PHP_EOL;
				}
			}
		unset($authors_list);
		unset($dms_posts);
		$select_author_text = __('Выберите автора','dms-business-russian');
		echo <<<EOF
			<div><select style="width:100%;" name="AUTHOR_ID">
				<option value="0">$select_author_text</option>
				$select_author_lists
			<select>
			</div>
EOF;
	} 

	public function metabox_post_views_show() { 
		//Подготавливаем данные для мета бокса
		global $post;
		$post_id = $post->ID;
		
		$POSTS_VIEWS = (int)get_post_meta( $post_id, '_POSTS_VIEWS', true);
		if( empty($POSTS_VIEWS) ) $POSTS_VIEWS = 0; 

		echo <<<EOF
			<div>
				<input style="width:100%;" type="number" name="POSTS_VIEWS" value="{$POSTS_VIEWS}"/>
			</div>
EOF;
	} 

	//Метобокс Для вовода названия страницы, которое будет отображаться над картинкой поста
	public function metabox_thumbnail_title_show() { 
		//Подготавливаем данные для мета бокса
		global $post;
		$post_id = $post->ID;
		
		$THUMBNAIL_TITLE = get_post_meta( $post_id, '_THUMBNAIL_TITLE', true);

		$thumbnail_text = __('Данные блок будит отображаться при условии выбора миниатюры записи.','dms-business-russian');
		$thumbnail_title_text = __('Название страницы','dms-business-russian');
		$thumbnail_description_text = __('Подпись под названием','dms-business-russian');
		$thumbnail_orientation_text = __('Расположение текста','dms-business-russian');
		$thumbnail_orientation_text_l = __('С лева','dms-business-russian');
		$thumbnail_orientation_text_c = __('По центру','dms-business-russian');
		$thumbnail_orientation_text_r = __('С права','dms-business-russian');
		$thumbnail_orientation_select_l = $thumbnail_orientation_select_c = $thumbnail_orientation_select_r = '';
		
		if( !isset($THUMBNAIL_TITLE['orientation']) ) $THUMBNAIL_TITLE['orientation'] = 'center';
		
		switch($THUMBNAIL_TITLE['orientation']){
			case 'left' : $thumbnail_orientation_select_l = ' checked="checked" '; break;
			case 'right' : $thumbnail_orientation_select_r = ' checked="checked" '; break;
			default: $thumbnail_orientation_select_c = ' checked="checked" '; break;
		}
		
		if( !isset($THUMBNAIL_TITLE['title']) ) $THUMBNAIL_TITLE['title'] = '';
		if( !isset($THUMBNAIL_TITLE['description']) ) $THUMBNAIL_TITLE['description'] = '';
		echo <<<EOF
			<table>
				<tr>
					<td colspan="2"><b>{$thumbnail_text}</b></td>
				</tr>
				<tr>
					<td>{$thumbnail_title_text}</td>
					<td><input type="text" style="width:100%;" name="THUMBNAIL_TITLE[title]" value="{$THUMBNAIL_TITLE['title']}"></td>
				</tr>
				<tr>
					<td>{$thumbnail_description_text}</td>
					<td><input type="text"  style="width:100%;" name="THUMBNAIL_TITLE[description]" value="{$THUMBNAIL_TITLE['description']}"></td>
				</tr>
				<tr>
					<td>{$thumbnail_orientation_text}</td>
					<td>
						<lable>
							<input type="radio" {$thumbnail_orientation_select_l} name="THUMBNAIL_TITLE[orientation]" value="left">
							{$thumbnail_orientation_text_l}
						<lable>
						<lable>
							<input type="radio" {$thumbnail_orientation_select_c} name="THUMBNAIL_TITLE[orientation]" value="center">
							{$thumbnail_orientation_text_c}
						<lable>
						<lable>
							<input type="radio" {$thumbnail_orientation_select_r} name="THUMBNAIL_TITLE[orientation]" value="right">
							{$thumbnail_orientation_text_r}
						<lable>
					</td>
				</tr>
			</table>
EOF;
	} 
		
	//Функция сохранения данных переданных от metabox авторов
	public function metabox_author_save( $post_id ) {
		/*Если нет переменной с массивом данных
			или автосохранение
			или у пользователя нет прав на редактирование записи,
		  То завершаем выполнение функции сохранения данных
		*/
		if( !isset( $_POST['AUTHOR_ID'] )
			|| ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
			|| !current_user_can( 'edit_posts', $post_id )
		) {
			return $post_id;
		}
		
		update_post_meta( $post_id, '_AUTHOR_ID',  (int)$_POST['AUTHOR_ID'] );
		update_post_meta( $post_id, '_POSTS_VIEWS',  (int)$_POST['POSTS_VIEWS'] );

		return $post_id;
	}
	
	//Функция сохранения данных переданных от metabox
	public function metabox_thumbnail_title_save( $post_id ) {
		/*Если нет переменной с массивом данных
			или данные не представлены массивом
			или автосохранение
			или у пользователя нет прав на редактирование записи,
		  То завершаем выполнение функции сохранения данных
		*/
		
		if( !isset( $_POST['THUMBNAIL_TITLE'] )
			|| !is_array( $_POST['THUMBNAIL_TITLE'] )
			|| ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
			|| !current_user_can( 'edit_posts', $post_id )
		) {
			return $post_id;
		}
		
		$THUMBNAIL_TITLE = array(
			'title' => '',
			'description' => '',
			'orientation' => 'center',
		);

		//Считываем переданные и данные по книги
		if( isset( $_POST['THUMBNAIL_TITLE']['title'] ) ) $THUMBNAIL_TITLE['title'] = esc_attr( $_POST['THUMBNAIL_TITLE']['title'] );
		if( isset( $_POST['THUMBNAIL_TITLE']['description'] ) ) $THUMBNAIL_TITLE['description'] = esc_attr( $_POST['THUMBNAIL_TITLE']['description'] );
		if( isset( $_POST['THUMBNAIL_TITLE']['orientation'] ) && in_array( $_POST['THUMBNAIL_TITLE']['orientation'], array('left', 'right', 'center') ) ) $THUMBNAIL_TITLE['orientation'] = $_POST['THUMBNAIL_TITLE']['orientation'];
	
		update_post_meta( $post_id, '_THUMBNAIL_TITLE',  $THUMBNAIL_TITLE );

		return $post_id;
	}
	//Функция сохранения подписи над миниатюрой поста
	public function metabox_book_parametr_save( $post_id ) {
		/*Если нет переменной с массивом данных
			или данные не представлены массивом
			или автосохранение
			или у пользователя нет прав на редактирование записи,
		  То завершаем выполнение функции сохранения данных
		*/
		if( !isset( $_POST['BOOK_PARAMETERS'] )
			|| !is_array( $_POST['BOOK_PARAMETERS'] )
			|| ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
			|| !current_user_can( 'edit_posts', $post_id )
		) {
			return $post_id;
		}
		
		$BOOK_PARAMETERS = $this->book_parameters_default;

		//Считываем переданные и данные по книги
		if( isset( $_POST['BOOK_PARAMETERS']['AUTHOR'] ) ) $BOOK_PARAMETERS['author'] = esc_attr( $_POST['BOOK_PARAMETERS']['AUTHOR'] );
		if( isset( $_POST['BOOK_PARAMETERS']['COST'] ) ) $BOOK_PARAMETERS['cost_val'] = esc_attr( $_POST['BOOK_PARAMETERS']['COST'] );
		if( isset( $_POST['BOOK_PARAMETERS']['COST_AKCIA'] ) ) $BOOK_PARAMETERS['cost_akcia_val'] = esc_attr( $_POST['BOOK_PARAMETERS']['COST_AKCIA'] );
		if( isset( $_POST['BOOK_PARAMETERS']['LEVEL'] ) ) $BOOK_PARAMETERS['level_val'] = esc_attr( $_POST['BOOK_PARAMETERS']['LEVEL'] );
		if( isset( $_POST['BOOK_PARAMETERS']['PAGES_COUNT'] ) ) $BOOK_PARAMETERS['pages_count_val'] = esc_attr( $_POST['BOOK_PARAMETERS']['PAGES_COUNT'] );
		if( isset( $_POST['BOOK_PARAMETERS']['FILE_SIZE'] ) ) $BOOK_PARAMETERS['file_size_val'] = esc_attr( $_POST['BOOK_PARAMETERS']['FILE_SIZE'] );
		if( isset( $_POST['BOOK_PARAMETERS']['PUBLISHER'] ) ) $BOOK_PARAMETERS['publisher_val'] = esc_attr( $_POST['BOOK_PARAMETERS']['PUBLISHER'] );
		if( isset( $_POST['BOOK_PARAMETERS']['PAYED_MARKET'] ) ) $BOOK_PARAMETERS['payed_market_val'] = esc_url( $_POST['BOOK_PARAMETERS']['PAYED_MARKET'] );
		if( isset( $_POST['BOOK_PARAMETERS']['PAYPAL_HOSTED_BUTTON_ID'] ) ) $BOOK_PARAMETERS['paypal_hosted_button_id'] = esc_attr( $_POST['BOOK_PARAMETERS']['PAYPAL_HOSTED_BUTTON_ID'] ); // ИД кнопки PayPal
		

		if( isset( $_POST['BOOK_PARAMETERS']['PAYPAL_SECRET_KEY'] )
			&& !empty( $_POST['BOOK_PARAMETERS']['PAYPAL_SECRET_KEY'] )
		) {  //секретный код для обратной ссылки при которой будет показываться кнопка скачать 
			$BOOK_PARAMETERS['paypal_secret_action'] = esc_attr( $_POST['BOOK_PARAMETERS']['PAYPAL_SECRET_KEY'] );
		}else{
			$BOOK_PARAMETERS['paypal_secret_action'] = rand(10000000, 100000000);
		}
		
		if( isset( $_POST['BOOK_PARAMETERS']['LINK_FOR_CONTENT_LOAD'] ) ) $BOOK_PARAMETERS['link_for_content_load'] = esc_url( $_POST['BOOK_PARAMETERS']['LINK_FOR_CONTENT_LOAD'] ); //ссылка для скачивания материалов
	
		update_post_meta( $post_id, '_BOOK_PARAMETERS',  $BOOK_PARAMETERS );

		return $post_id;
	}
	
	function __construct() {
		\add_meta_box('metabox_thumbnail_title_show', __('Заголовок страницы над миниатюрой','dms-business-russian'), array( &$this, 'metabox_thumbnail_title_show'), array( 'page'), 'normal', 'high'); 
		\add_meta_box('post_author', __('Автор','dms-business-russian'), array( &$this, 'metabox_author_show'), array( 'post', 'store', 'library' ), 'side', 'high'); 
		\add_meta_box('book_parameters', __('Параметры книги','dms-business-russian'), array( &$this, 'metabox_book_show'), 'books', 'side', 'high'); 
		\add_meta_box('posts_views', __('Кол.просмотров','dms-business-russian'), array( &$this, 'metabox_post_views_show'), array( 'post', 'store', 'library' ), 'side', 'high'); 
		add_action('save_post', array( &$this, 'metabox_book_parametr_save'));
		add_action('save_post', array( &$this, 'metabox_author_save'));
		add_action('save_post', array( &$this, 'metabox_thumbnail_title_save'));
	}
}

function call_METABOXES( $arg ){
	new  \DStaroselskiy\Theme\Business_Russian\METABOXES();
}

if ( is_admin() ) {
	add_action( 'load-post.php', '\DStaroselskiy\Theme\Business_Russian\call_METABOXES');
	add_action( 'load-post-new.php', '\DStaroselskiy\Theme\Business_Russian\call_METABOXES');
}
?>