<?php

/* 
 * Сборник полезных функций для Вордпресса
 * 
 * 1) DMS- вывод похожих записей по категории и тегам поста
 * 
 */
namespace DMS\Wordpress;

class Library {
    static function get_related_posts( $post_id = 0, $taxonomys = false, $category_ids = false, $tag_ids = false, $echo = true, $tamplate = 'related_post.tpl')
    {           
        $post_type = 'post';
        if( (int)$post_id > 0 )
        {
           $post_id = (int)$post_id; 
        }
        else
        {
            global $post;
            $post_id = (int)$post->ID;
            $post_type = $post->post_type;
        }
        $str_posts = '';        
        $search_arg = array(
            'posts_per_page' => 4, // Количество вывода похожих статей.
            'ignore_sticky_posts' => 1,
            'post__not_in' => array( $post_id ),  
            'post_status'  => "publish",
            'post_type' => $post_type,
        );    
        if( $taxonomys === true )
        {
            
        }
        //если передан параметр для авто апределения категорий к которым пренадлежит запись, 
        //то считывает все категории этой записи в массив
        if( $category_ids === true ) 
        {
            $category_ids = array();
            $categories = \get_the_category( $post_id );
            var_dump( $categories );
            if( $categories ) 
            {
                foreach($categories as $category) 
                {
                    $category_ids[] = $category->term_id;
                }
            }
        }
        if( ( is_array( $category_ids ) && ( count( $category_ids ) > 0 ) )
            || ( (int)$category_ids > 0 ) )
        {
            $search_arg['category__in'] = $category_ids;
        }
        //если передан параметр для авто апределение тегов записи, 
        //то считывает все теги этой записи в массив
        if( $tag_ids === true ) 
        {
            $tag_ids = array();    
            $tags = \wp_get_post_tags( $post_id );
            if ($tags) 
            {
                foreach( $tags as $tag) 
                {
                    $tag_ids[] = $tag->term_id;
                }
            }
        }        
        if( ( is_array( $tag_ids ) && ( count( $tag_ids ) > 0 ) )
                || ( (int)$tag_ids > 0 ) )
        {
            $search_arg['tag__in'] = $tag_ids;
        }        
        var_dump( $search_arg );
        die();
        $my_query = new \WP_Query( $search_arg );
        if( $my_query->have_posts() ) 
        {
            $template = '';
            if( file_exists( $template ) )
            {
                $template = file_get_contents( $template );
            }
            if( empty( $template ) ) return false;
            foreach($my_query->posts as $post )
            {
//                $template = \str_replace( 
//                    array(
//                        '*|ID|*',
//                        '*|TITLE|*',
//                        '*|CONTENT|*',
//                    ),
//                    array(
//                       $post->ID,
//                       $post->post_date,     
//                       $post->post_date_gmt,     
//                       $post->post_content,     
//                       $post->post_title,     
//                       $post->post_excerpt,     
//                       $post->post_name,     
//                       $post->post_name,     
//                       $post->post_type,     
//                    ), 
//                    $template
//                );
                var_dump( $post );
            }
            $str_posts = '<div class="dms_related_posts">'.$str_posts.'</div>';
            if( $echo ) {
                echo $str_posts;
            }
        }
        return $str_posts;
    }
}
