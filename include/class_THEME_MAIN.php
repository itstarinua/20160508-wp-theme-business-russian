<?php
/*********************************************
Описание основного класса управления темой
by DStaroselskiy 

Version: 0.1
Date: 2016-05-03
*********************************************/
namespace DStaroselskiy\Theme\Business_Russian;

if( !class_exists('\DStaroselskiy\Theme\Business_Russian\COSTOMIZER') ) { require_once( DMS_THEME_DIR . '/include/class_COSTOMIZER.php'); }
if( !class_exists('\DStaroselskiy\Theme\Business_Russian\METABOXES') ) { require_once( DMS_THEME_DIR . '/include/class_METABOXES.php'); }
if( !class_exists('\DStaroselskiy\Theme\Business_Russian\GET_RESPONSE_FORM') ) { require_once( DMS_THEME_DIR . '/include/class_GET_RESPONSE_FORM.php'); }

class THEME_MAIN extends \DStaroselskiy\Theme\Business_Russian\COSTOMIZER {
	
	
	// Подключаем размер дополнительных миниатюр к админке
	function thumbnails_custom_sizes( $sizes ) {
		return array_merge( $sizes, array(
			//'category-thumb' => 'Мой размерчик',
			//'homepage-thumb' => __(''),
			'catalog-thumb-min' => __('Catalog thumbnails min','dms-business-russian'),
			'catalog-thumb' => __('Catalog thumbnails standart','dms-business-russian'),
			'catalog-thumb-max' => __('Catalog thumbnails max','dms-business-russian'),
		) );		
	}
	
	// Регистрируем два сайдбара для вівода виджетов с параметрами:  Один с float:left, другой с float:right 
	function register_sidebars(){
		if ( function_exists( 'register_sidebar' ) ) {
			register_sidebar( array(
				'name' => __('Left sidebar in footer','dms-business-russian'),
				'id' => "sidebar-footer-left",
				'description' => '',
				'class' => '',
				'before_widget' => '<li id="%1$s" class="widget %2$s">',
				'after_widget' => "</li>\n",
				'before_title' => '<h2 class="widgettitle">',
				'after_title' => "</h2>\n",
			) );
			register_sidebar( array(
				'name' => __('Right sidebar in footer','dms-business-russian'),
				'id' => "sidebar-footer-right",
				'description' => '',
				'class' => '',
				'before_widget' => '<li id="%1$s" class="widget %2$s">',
				'after_widget' => "</li>\n",
				'before_title' => '<h5 class="widgettitle">',
				'after_title' => "</h5>\n",
			) );
			register_sidebar( array(
				'name' => __('Right sidebar in content','dms-business-russian'),
				'id' => "sidebar-content-right",
				'description' => '',
				'class' => '',
				'before_widget' => '<li id="%1$s" class="widget %2$s">',
				'after_widget' => "</li>\n",
				'before_title' => '<h5 class="widgettitle">',
				'after_title' => "</h5>\n",
			) );
			register_sidebar( array(
				'name' => __('Footer caregory sidebar','dms-business-russian'),
				'id' => "sidebar-category-footer",
				'description' => '',
				'class' => '',
				'before_widget' => '<li id="%1$s" class="widget %2$s">',
				'after_widget' => "</li>\n",
				'before_title' => '<div><h5 class="widgettitle theme-page-h2">',
				'after_title' => "</h5></div>\n",
			) );
		}
	}
	//Подключаем файлы стилей
	public function load_style_links(){
		if( !wp_style_is( 'font-awesome', 'registered ') ) wp_enqueue_style( 'font-awesome', DMS_THEME_URL.'/css/font-awesome.min.css', array(), null ,'all');
		if( !wp_style_is( 'dms-business-russian-fonts', 'registered ') ) wp_enqueue_style( 'dms-business-russian-fonts', DMS_THEME_URL.'/fonts.css', array(), null ,'all');
		if( !wp_style_is( 'dms-business-russian-core', 'registered ') ) wp_enqueue_style( 'dms-business-russian-core', DMS_THEME_URL.'/css/core.css', array(), null ,'all');
		if( !wp_style_is( 'dms-business-russian-style', 'registered ') ) wp_enqueue_style( 'dms-business-russian-style', DMS_THEME_URL.'/style.css', array(), null ,'all');
		if( !wp_style_is( 'vc_typicons', 'registered ') ) wp_enqueue_style( 'vc_typicons', DMS_THEME_URL.'/font/typicons.min.css', array(), null ,'all');
	}
	//Подключаем файлы скриптов
	public function load_script_links(){
		if( !wp_script_is( 'jquery', 'registered ') ) wp_enqueue_script( 'jquery', DMS_THEME_URL.'/js/jquery-1.11.3.min.js',array(),'1.11.3',true);
		if( !wp_script_is( 'jquery.inputmask', 'registered ') ) wp_enqueue_script( 'jquery.inputmask', DMS_THEME_URL.'/js/jquery.inputmask.js',array('jquery'),null,true);
		if( !wp_script_is( 'jquery.inputmask.extensions', 'registered ') ) wp_enqueue_script( 'jquery.inputmask.extensions', DMS_THEME_URL.'/js/jquery.inputmask.extensions.js',array('jquery'),null,true);
		if( !wp_script_is( 'jquery.inputmask.regex.extensions', 'registered ') ) wp_enqueue_script( 'jquery.inputmask.regex.extensions', DMS_THEME_URL.'/js/jquery.inputmask.regex.extensions.js',array('jquery'),null,true);
		if( !wp_script_is( 'dms-business-russian-core', 'registered ') ) wp_enqueue_script( 'dms-business-russian-core', DMS_THEME_URL.'/js/core.js',array('jquery'),null,true);
	}

	public function theme_setup(){
		//Задаем размер дополнительных миниатюр
		if ( function_exists( 'add_image_size' ) ) {
			\add_image_size('book', 225, 291, true);
			\add_image_size('book-small', 133, 169, true);
			\add_image_size('author', 185, 185, true);
			\add_image_size('avatar-small', 66, 66, true);
			\add_image_size('categiry-small', 279, 181, true);
		}
		
		\register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'dms-business-russian' ),
			'social'  => __( 'Social Links Menu', 'dms-business-russian' ),
		) );

		\load_theme_textdomain( 'dms-business-russian', \get_template_directory() . '/languages' );
		
		// Инициализация настроек темы
		if ( function_exists( 'add_theme_support' ) ) {
		
			\add_theme_support( 'title-tag' );

			\add_theme_support( 'custom-logo', array(
				'height'      => 225,
				'width'       => 172,
				'flex-height' => true,
			) );
			
			\add_theme_support( 'custom-header', array(
				'width'         => 1920,
				'height'        => 745,
				'flex-width'    => true,
				'flex-height'    => true,
				'uploads'       => true,
			));

			\add_theme_support('post-thumbnails'); 
		
			\add_theme_support( 'html5', array('search-form','comment-form','comment-list','gallery','caption',) );

			\add_theme_support( 'post-formats', array('image','video',) );
			// add_theme_support( 'post-formats', array('aside','image','video','quote','link','gallery','status',	'audio','chat',) );
		}
		
		add_editor_style( array( 'css/editor-style.css', DMS_THEME_URL ) );

		add_theme_support( 'customize-selective-refresh-widgets' );	
	}
	public function add_post_type(){	

		//Добавляем раздел - произвольный тип записей - ПАРТНЕРЫ
		$labels = array(
			'name'          => __('Партнеры','dms-business-russian'), // основное название для типа записи
			'singular_name' => __('Партнер','dms-business-russian'), // название для одной записи этого типа
			'add_new'       => __('Добавить партнера','dms-business-russian'), // для добавления новой записи
			'add_new_item'  => __('Добавить нового партнера','dms-business-russian'), // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'     => __('Изменить данные партнера','dms-business-russian'), // для редактирования типа записи
			'new_item'      => __('Новый партнер','dms-business-russian'), // текст новой записи
			'view_item'     => __('Просмотр данных партнера','dms-business-russian'), // для просмотра записи этого типа.
			'search_items'  => __('Найти партнера','dms-business-russian'), // для поиска по этим типам записи
			'not_found'     => __('Партнер не найден','dms-business-russian'), // если в результате поиска ничего не было найдено
			'not_found_in_trash' => __('Партнер в карзине не найден','dms-business-russian'), // если не было найдено в корзине
			'parent_item_colon'  => '', // для родительских типов. для древовидных типов
			'menu_name'          => __('Партнеры','dms-business-russian'), // название меню
		);			
		$args = array(
			'labels' => $labels,
			'description'  => __('Список партнераов','dms-business-russian'),
			'public' => false,
			'exclude_from_search' => true, //Исключить из поиска
			'publicly_queryable' => true, //Запросы относящиеся к этому типу записей будут работать во фронтэнде (в шаблоне сайта).
			'show_ui' => true,//false, //Показывать ли меню для управления этим типом записи в админ-панели.
			'show_in_menu' => true,//false, //Показывать ли тип записи в администраторском меню и где именно показывать управление этим типом записи. Аргумент show_ui должен быть включен!
			'query_var' => true,
			'rewrite' => false, //Использовать ли ЧПУ для этого типа записи. 
			'capability_type' => 'post',
			'has_archive' => false,//true, //Включить поддержку страниц архивов для этого типа записей
			'hierarchical' => false,
			'menu_position' => null,
			'show_in_nav_menus' => true, //Включить возможность выбирать этот тип записи в меню навигации.
			'supports' => array('title','editor','thumbnail'),
			// '_builtin' => true,
			'_edit_link' => 'post.php?post=%d&post_type=our-partners',
		);
		register_post_type('our-partners', $args );
		//Добавляем раздел - произвольный тип записей - АВТОР
		$labels = array(
			'name'          => __('Авторы','dms-business-russian'), // основное название для типа записи
			'singular_name' => __('Автор','dms-business-russian'), // название для одной записи этого типа
			'add_new'       => __('Добавить автора','dms-business-russian'), // для добавления новой записи
			'add_new_item'  => __('Добавить нового автора','dms-business-russian'), // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'     => __('Изменить данные автора','dms-business-russian'), // для редактирования типа записи
			'new_item'      => __('Новый автор','dms-business-russian'), // текст новой записи
			'view_item'     => __('Просмотр данных автора','dms-business-russian'), // для просмотра записи этого типа.
			'search_items'  => __('Найти автора','dms-business-russian'), // для поиска по этим типам записи
			'not_found'     => __('Автор не найден','dms-business-russian'), // если в результате поиска ничего не было найдено
			'not_found_in_trash' => __('Автор в карзине не найден','dms-business-russian'), // если не было найдено в корзине
			'parent_item_colon'  => '', // для родительских типов. для древовидных типов
			'menu_name'          => __('Авторы','dms-business-russian'), // название меню
		);			
		$args = array(
			'labels' => $labels,
			'description'  => __('Список авторов','dms-business-russian'),
			'public' => false,
			'exclude_from_search' => true, //Исключить из поиска
			'publicly_queryable' => true, //Запросы относящиеся к этому типу записей будут работать во фронтэнде (в шаблоне сайта).
			'show_ui' => true,//false, //Показывать ли меню для управления этим типом записи в админ-панели.
			'show_in_menu' => true,//false, //Показывать ли тип записи в администраторском меню и где именно показывать управление этим типом записи. Аргумент show_ui должен быть включен!
			'query_var' => true,
			'rewrite' => false, //Использовать ли ЧПУ для этого типа записи. 
			'capability_type' => 'post',
			'has_archive' => false,//true, //Включить поддержку страниц архивов для этого типа записей
			'hierarchical' => false,
			'menu_position' => null,
			'show_in_nav_menus' => true, //Включить возможность выбирать этот тип записи в меню навигации.
			'supports' => array('title','editor','thumbnail'),
			// '_builtin' => true,
			'_edit_link' => 'post.php?post=%d&post_type=our-author',
		);
		register_post_type('our-author', $args );
		//Добавляем раздел - произвольный тип записей - Книги Store - Магазин
		$labels = array(
			'name'          => __('Магазин','dms-business-russian'), // основное название для типа записи
			'singular_name' => __('Книга','dms-business-russian'), // название для одной записи этого типа
			'add_new'       => __('Добавить книгу','dms-business-russian'), // для добавления новой записи
			'add_new_item'  => __('Добавить новую книгу','dms-business-russian'), // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'     => __('Изменить данные книги','dms-business-russian'), // для редактирования типа записи
			'new_item'      => __('Новая книга','dms-business-russian'), // текст новой записи
			'view_item'     => __('Просмотр данные книги','dms-business-russian'), // для просмотра записи этого типа.
			'search_items'  => __('Найти книгу','dms-business-russian'), // для поиска по этим типам записи
			'not_found'     => __('Книга не найден','dms-business-russian'), // если в результате поиска ничего не было найдено
			'not_found_in_trash' => __('Книга в карзине не найден','dms-business-russian'), // если не было найдено в корзине
			'parent_item_colon'  => '', // для родительских типов. для древовидных типов
			'menu_name'          => __('Магазин','dms-business-russian'), // название меню
		);
			
		$args = array(
			'labels' => $labels,
			'description'  => __('Список книг','dms-business-russian'),
			'public' => true,
			'exclude_from_search' => false, //Исключить из поиска
			'publicly_queryable' => true, //Запросы относящиеся к этому типу записей будут работать во фронтэнде (в шаблоне сайта).
			'show_ui' => true,//false, //Показывать ли меню для управления этим типом записи в админ-панели.
			'show_in_menu' => true,//false, //Показывать ли тип записи в администраторском меню и где именно показывать управление этим типом записи. Аргумент show_ui должен быть включен!
			'query_var' => true,
			'rewrite' => array( //Использовать ли ЧПУ для этого типа записи. 
				'slug' => 'store',				
			),
			'capability_type' => 'post',
			'has_archive' => true,//true, //Включить поддержку страниц архивов для этого типа записей
			'hierarchical' => false,
			'menu_position' => null,
			'show_in_nav_menus' => true, //Включить возможность выбирать этот тип записи в меню навигации.
			'supports' => array('title','editor','thumbnail', 'post-formats', 'excerpt', 'comments'),

			// '_builtin' => true,
			'_edit_link' => 'post.php?post=%d&post_type=books',
		);
		register_post_type('books', $args );
		//Таксономия категорий книг
		$labels = array(
			'name'              => __('Категория книг','dms-business-russian'),
			'singular_name'     => __('Категория книг','dms-business-russian'),
			'search_items'      => __('Найти категорию книг','dms-business-russian'),
			'all_items'         => __('Все категории книг','dms-business-russian'),
			'parent_item'       => __('Родительская категория','dms-business-russian'),
			'parent_item_colon' => __('Родительская категория:','dms-business-russian'),
			'edit_item'         => __('Редактировать категорию','dms-business-russian'),
			'update_item'       => __('Обновить категорию','dms-business-russian'),
			'add_new_item'      => __('Добавить новую категорию','dms-business-russian'),
			'new_item_name'     => __('Име новой категории','dms-business-russian'),
			'menu_name'         => __('Категории книг','dms-business-russian'),
		); 
		// параметры
		$args = array(
			'label'                 => __('Категории','dms-business-russian'), // определяется параметром $labels->name
			'labels'                => $labels,
			'public'                => true,
			'publicly_queryable'    => null, // равен аргументу public
			'show_in_nav_menus'     => true, // равен аргументу public
			'show_ui'               => true, // равен аргументу public
			'show_tagcloud'         => true, // равен аргументу show_ui
			'hierarchical'          => true,
			'update_count_callback' => '',
			'rewrite'               => true,
			//'query_var'             => $taxonomy, // название параметра запроса
			'capabilities'          => array(),
			'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
			'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
			'_builtin'              => false,
			'show_in_quick_edit'    => null, // по умолчанию значение show_ui
		);
		register_taxonomy('category_of_books', array('books'), $args );
		//Добавляем раздел - произвольный тип записей - Library
		$labels = array(
			'name'          => __('Библиотека','dms-business-russian'), // основное название для типа записи
			'singular_name' => __('История','dms-business-russian'), // название для одной записи этого типа
			'add_new'       => __('Добавить историю','dms-business-russian'), // для добавления новой записи
			'add_new_item'  => __('Добавить новую историю','dms-business-russian'), // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'     => __('Изменить данные истории','dms-business-russian'), // для редактирования типа записи
			'new_item'      => __('Новая история','dms-business-russian'), // текст новой записи
			'view_item'     => __('Просмотр данные истории','dms-business-russian'), // для просмотра записи этого типа.
			'search_items'  => __('Найти историю','dms-business-russian'), // для поиска по этим типам записи
			'not_found'     => __('История не найден','dms-business-russian'), // если в результате поиска ничего не было найдено
			'not_found_in_trash' => __('История в карзине не найден','dms-business-russian'), // если не было найдено в корзине
			'parent_item_colon'  => '', // для родительских типов. для древовидных типов
			'menu_name'          => __('Библиотека','dms-business-russian'), // название меню
		);
			
		$args = array(
			'labels' => $labels,
			'description'  => __('Список историй в библиотеке','dms-business-russian'),
			'public' => true,
			'exclude_from_search' => false, //Исключить из поиска
			'publicly_queryable' => true, //Запросы относящиеся к этому типу записей будут работать во фронтэнде (в шаблоне сайта).
			'show_ui' => true,//false, //Показывать ли меню для управления этим типом записи в админ-панели.
			'show_in_menu' => true,//false, //Показывать ли тип записи в администраторском меню и где именно показывать управление этим типом записи. Аргумент show_ui должен быть включен!
			'query_var' => true,
			'rewrite' => true, //Использовать ли ЧПУ для этого типа записи. 
			'capability_type' => 'post',
			'has_archive' => true,//true, //Включить поддержку страниц архивов для этого типа записей
			'hierarchical' => false,
			'menu_position' => null,
			'show_in_nav_menus' => true, //Включить возможность выбирать этот тип записи в меню навигации.
			'supports' => array('title','editor','thumbnail', 'excerpt', 'comments'),
			// '_builtin' => true,
			'_edit_link' => 'post.php?post=%d&post_type=library',
		);
		register_post_type('library', $args );
		//Таксономия категорий книг
		$labels = array(
			'name'              => __('Категория исотрии','dms-business-russian'),
			'singular_name'     => __('Категория истории','dms-business-russian'),
			'search_items'      => __('Найти категорию истории','dms-business-russian'),
			'all_items'         => __('Все категории историй','dms-business-russian'),
			'parent_item'       => __('Родительская категория','dms-business-russian'),
			'parent_item_colon' => __('Родительская категория:','dms-business-russian'),
			'edit_item'         => __('Редактировать категорию','dms-business-russian'),
			'update_item'       => __('Обновить категорию','dms-business-russian'),
			'add_new_item'      => __('Добавить новую категорию','dms-business-russian'),
			'new_item_name'     => __('Име новой категории','dms-business-russian'),
			'menu_name'         => __('Категории историй','dms-business-russian'),
		); 
		// параметры
		$args = array(
			'label'                 => __('Категории','dms-business-russian'), // определяется параметром $labels->name
			'labels'                => $labels,
			'public'                => true,
			'publicly_queryable'    => null, // равен аргументу public
			'show_in_nav_menus'     => true, // равен аргументу public
			'show_ui'               => true, // равен аргументу public
			'show_tagcloud'         => true, // равен аргументу show_ui
			'hierarchical'          => true,
			'update_count_callback' => '',
			'rewrite'               => true,
			//'query_var'             => $taxonomy, // название параметра запроса
			'capabilities'          => array(),
			'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
			'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
			'_builtin'              => false,
			'show_in_quick_edit'    => null, // по умолчанию значение show_ui
		);
		register_taxonomy('category_of_stores', array('library'), $args );
	}
	public function our_partners_do_shortcode( $atts ){
		if( !isset($atts['numberposts']) ||  ( (int)$atts['numberposts'] < -1) ) $atts['numberposts'] = -1;
		$our_partners = get_posts( array(
			'numberposts'     => (int)$atts['numberposts'], // тоже самое что posts_per_page
			'offset'          => 0,
			'orderby'         => 'post_date',
			'order'           => 'DESC',
			'post_type'       => 'our-partners',
			'post_status'     => 'publish',
			'nopaging'     => true,
		) );
		// var_dump("our_partners_do_shortcode");
		// var_dump($our_partners);
		$output_str = '';
		$i = 0;
		foreach($our_partners as $our_partner){ 
			// setup_postdata($our_partner);			
			// if( has_post_thumbnail() ) {
				$partner_logo_url = get_the_post_thumbnail($our_partner, array(171,82), array(
					'class'=> "our-partner-logo attachment-$size",
					'alt'=> "Logo of ".trim(strip_tags( $partner_name )),
					'title'=> trim(strip_tags( $partner_name )),
				));				
				$i +=1;
		    	$output_str .= <<<EOF
		    	<li class="our-partner-point our-partner-id-$$our_partner->ID our-partner-slug-$our_partner->post_name">
		    		$partner_logo_url 
		    		<p class="our-partner-title">$our_partner->post_title</p>
		    	</li>
EOF;
			// }
		}
		// wp_reset_postdata();
		return <<<EOL
		<ul class="our-partner-lists our-partners-count-$i">
			$output_str
		</ul>
EOL;
	}
	public function our_books_do_shortcode($arg){
		$books = get_posts( array(
			'post_type'		=> 'books',
			'numberposts' 	=> 4,					
		) );
		$return_str = '';
		if( count($books) > 0 ) {
			foreach( $books as $book ) { 
				$the_permalink = get_the_permalink($book->ID);
				$the_title = $book->post_title;
				$the_link_title = sprintf( __("Читать о %s","dms-business-russian"), $book->post_title );
				$the_post_thumbnail = '';
				if( has_post_thumbnail($book->ID) ) {
					$the_post_thumbnail = get_the_post_thumbnail($book->ID, 'book-small', array( 'class' => "attachment attachment-book book-small") ); 
				} else { 
					$the_post_thumbnail = '<div class="attachment-book size-book wp-post-image no-photo-133x169"></div>';
				} 
				$ths_book_level = '';
				$BOOK_PARAMETERS = get_post_meta(  $book->ID , '_BOOK_PARAMETERS', true); 
				if( !empty( $BOOK_PARAMETERS['level_val'] ) ) { 
					$ths_book_level = <<<EOF
					<div class="book-level">
						{$BOOK_PARAMETERS['level_val']}
					</div>
EOF;
				}
				$return_str .= <<<EOF
					<div class="book-information">
						<a href="{$the_permalink}" title="{$the_link_title}">
							{$the_post_thumbnail}
							<h6 class="book-title">
								{$the_title}
							</h6>
							{$ths_book_level} 
						</a>
					</div>	
EOF;
			}
			return <<<EOF
			<div id="books-list"> 							
				$return_str
			</div>			
EOF;
		}
		return ''; 
	}

	public function post_views( $the_ID ) {
		return (int)get_post_meta( $the_ID, '_POSTS_VIEWS', true);
	}
	public function post_views_count() {
		if( is_admin() ) return false;
		global $post;
		$the_ID = $post->ID;
		$POSTS_VIEWS = (int)get_post_meta( $the_ID, '_POSTS_VIEWS', true);
		if( empty($POSTS_VIEWS) ) $POSTS_VIEWS = 0; 
		if( !isset( $_COOKIE[ "POSTS_VIEWS_".$the_ID ] ) ) {			
			$POSTS_VIEWS = $POSTS_VIEWS + 1; 
			update_post_meta( $the_ID, '_POSTS_VIEWS', ( $POSTS_VIEWS ) );
			setcookie( "POSTS_VIEWS_".$the_ID, 'true', time()+622080000, '/' );
		}
		return $POSTS_VIEWS;
	}

	function __construct() {
		
		parent::__construct();
		//Очищаем заготовок CMS
		remove_action( 'wp_head', 'feed_links_extra', 3 ); 
		remove_action( 'wp_head', 'feed_links', 2 );
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'index_rel_link' );
		remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); 
		remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
		remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
		remove_action( 'wp_head', 'wp_generator' );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );
		//Удаляем "манифест". Он подключает специальный XML-файл к сайту,  чтобы включить возможность взаимодействия блога на движке WordPress с приложением Windows Live Writer.
		remove_action('wp_head', 'wlwmanifest_link');
		
		add_action( 'after_setup_theme', array( &$this, 'theme_setup') );	
		add_action('init', array( &$this, 'add_post_type'), 100, 0 );
		add_action( 'wp_enqueue_scripts', array( &$this, 'load_style_links') );//Подключаем вывод основных стилей
		add_action( 'wp_enqueue_scripts', array( &$this, 'load_script_links') );//Подключаем вывод основных скриптов
		// add_filter( 'locale', array( &$this, 'set_stella_locale' ) ); //Корректируем локализацию темы от плагина STELLA
		// add_filter( 'stella_lang_name', array( &$this, 'change_lang_names' ) );//Изменяем отображение языков у плагина STELLA
		add_filter( 'image_size_names_choose', array( &$this, 'thumbnails_custom_sizes' ) );// Подключаем размер дополнительных миниатюр к админке
		add_action( 'widgets_init', array( &$this,  'register_sidebars' ) ); // Регистрируем два сайдбара для вівода виджетов с параметрами:  Один с float:left, другой с float:right 
		
		add_filter( 'show_post_views', array( &$this, 'post_views' ), 10 ,1 );
		add_action( 'wp', array( &$this, 'post_views_count' ), 10 ,0 );
		
		add_shortcode('OUR_PARTNERS', array( &$this, 'our_partners_do_shortcode'));
		add_shortcode('OUR_BOOKS', array( &$this, 'our_books_do_shortcode'));

	}
}

?>